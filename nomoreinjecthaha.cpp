#include <mysql_driver.h>
#include <mysql_connection.h>
#include <cppconn/connection.h>
#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/prepared_statement.h>

#include <iostream>
#include <memory>
#include <string>
#include <tuple>
#include <initializer_list>

class CQueryBuilder
{
public:
	CQueryBuilder();
	CQueryBuilder(const char* base);
	CQueryBuilder(const std::string& base);

	std::string Select(const std::initializer_list<std::string>& collums, 
			   const std::string& table, 
			   const std::initializer_list<std::string>& constraints = std::initializer_list<std::string>(), 
			   const std::initializer_list<std::string>& constraintValues = std::initializer_list<std::string>(), 
			   const std::string& order = "", 
			   int limit = 0);

	std::string Insert(std::string table,
			   const std::initializer_list<std::string>& collums,
			   const std::initializer_list<std::string>& values);

	std::string Update(std::string table,
			   const std::initializer_list<std::string>& collums,
			   const std::initializer_list<std::string>& values,
			   const std::initializer_list<std::string>& constraints = std::initializer_list<std::string>(),
   			   const std::initializer_list<std::string>& constraintValues = std::initializer_list<std::string>());

	std::string Delete(std::string table,
			   const std::initializer_list<std::string>& collums,
			   const std::initializer_list<std::string>& values);

	std::string getQuery();	
	void clear();
private:
	
	void append(char* toAppend);
	void append(std::string toAppend);
	template<typename T> void append(const std::initializer_list<T>& toAppend);
	template<typename T> void appendParameters(const std::initializer_list<T>& parameters, const std::initializer_list<T>& parameterValues); 
	template<typename T> void appendConstraints(const std::initializer_list<T>& constraints, const std::initializer_list<T>& constraintValues); 
	
	std::string m_Query;
};

CQueryBuilder::CQueryBuilder(): m_Query(std::string("")){}
CQueryBuilder::CQueryBuilder(const char* base): m_Query(base){}
CQueryBuilder::CQueryBuilder(const std::string& base): m_Query(base){}

void CQueryBuilder::append(char* toAppend) {m_Query += toAppend; m_Query += ' ';}
void CQueryBuilder::append(std::string toAppend) {m_Query += toAppend; m_Query += ' ';}
template<typename T> void CQueryBuilder::append(const std::initializer_list<T>& toAppend) 
{
	bool writeDash = false;
	for(auto& i: toAppend){
		if(writeDash) 
			m_Query += ", ";
		m_Query += std::string(i);
	
		writeDash = true;
	}
	m_Query += ' ';
}

template<typename T> void CQueryBuilder::appendParameters(const std::initializer_list<T>& parameters, const std::initializer_list<T>& parameterValues)
{
	if(parameters.size() != parameterValues.size())
		return;
	
	auto c = parameters.begin();
	auto v = parameterValues.begin();
	
	bool writeDash = false;
	
	for(int i = 0; i < parameters.size(); ++i){
		if(writeDash)
			m_Query += " , ";
		
		m_Query += *(c + i);
		m_Query += " = ";
		m_Query += *(v + i);
		writeDash = true;	
	}
	m_Query += " ";

} 

template<typename T> void CQueryBuilder::appendConstraints(const std::initializer_list<T>& constraints, const std::initializer_list<T>& constraintValues) 
{
	if(constraints.size() != constraintValues.size())
		return;
	
	auto c = constraints.begin();
	auto v = constraintValues.begin();
	bool writeAND = false;
	
	for(int i = 0; i < constraints.size(); ++i){
		if(writeAND)
			m_Query += " AND ";
		
		m_Query += *(c + i);
		m_Query += " ";
		m_Query += (*(v + i))[0];
		m_Query += " ?";
		writeAND = true;	
	}
	m_Query += " ";
}

std::string CQueryBuilder::getQuery() {return m_Query;}

void CQueryBuilder::clear() {m_Query.clear();}

std::string CQueryBuilder::Select(const std::initializer_list<std::string>& collums, 
			    	  const std::string& table, 
			    	  const std::initializer_list<std::string>& constraints, 
			    	  const std::initializer_list<std::string>& constraintValues, 
			    	  const std::string& order, 
			    	  int limit)
{
	clear();
	append(std::string("SELECT"));
	append(collums);
	append(std::string("FROM"));
	append(table);
	
	if(0 != constraints.size()){
		append(std::string("WHERE"));
		appendConstraints(constraints, constraintValues);
	}
	
	if("" != order){
		append(std::string("ORDER BY ") + order);
	}
	
	if(0 != limit){
		append(std::string("LIMIT ") + std::to_string(limit));
	}
	
	return m_Query;
}

std::string CQueryBuilder::Insert(std::string table,
				  const std::initializer_list<std::string>& collums,
				  const std::initializer_list<std::string>& values)
{
	if(collums.size() != values.size()) return "";	//throw

	clear();
	append(std::string("INSERT INTO"));
	append(table);
	append(std::string("("));
	append(collums);
	append(std::string(")"));
	append(std::string("VALUES"));
	append(std::string("("));
	bool writeComma = false;
	for(auto& i: values) {
		if(writeComma)
			append(std::string(","));
		append(std::string("?"));
		writeComma = true;
	}
	append(std::string(")"));
	
	return m_Query;
}

std::string CQueryBuilder::Update(std::string table,
		   		  const std::initializer_list<std::string>& collums,
			          const std::initializer_list<std::string>& values,
				  const std::initializer_list<std::string>& constraints,
				  const std::initializer_list<std::string>& constraintValues)
{
	if(collums.size() != values.size()) return "";
	if(constraints.size() != constraintValues.size()) return "";
	
	clear();
	append(std::string("UPDATE"));
	append(table);
	append(std::string("SET"));
	bool writeComma = false;
	for(auto& i: collums) {
		if(writeComma)
			append(std::string(","));
		append(i);
		append(std::string("= ?"));
		writeComma = true;
	}

	
	if(0 != constraints.size()){
		append(std::string("WHERE"));
		appendConstraints(constraints, constraintValues);
	}
		
	return m_Query;
}	 

std::string CQueryBuilder::Delete(std::string table,
				  const std::initializer_list<std::string>& collums,
				  const std::initializer_list<std::string>& values)
{
	clear();
	append(std::string("DELETE FROM"));
	append(table);
	if(0 != values.size()){						//TODO: podivat se na hodnoty, abych pak uzivateli neumoznil udelat neco jako "{}"
		append(std::string("WHERE"));
		appendConstraints(collums, values);
	}
	
	return m_Query;
}

class CMySQLConnectionInterface
{
public:
	CMySQLConnectionInterface(const std::string& IP, const std::string& username, const std::string& pswd);
	~CMySQLConnectionInterface();
	void setSchema(const std::string& schema);
	
	std::string Select(const std::initializer_list<std::string>& collums, 
			   const std::string& table, 
			   const std::initializer_list<std::string>& constraints = std::initializer_list<std::string>(), 
			   const std::initializer_list<std::string>& constraintValues = std::initializer_list<std::string>(), 
			   const std::string& order = "", 
			   int limit = 0);

	std::string Insert(std::string table,
			   const std::initializer_list<std::string>& collums,
			   const std::initializer_list<std::string>& values);

	std::string Update(std::string table,
			   const std::initializer_list<std::string>& collums,
			   const std::initializer_list<std::string>& values,
			   const std::initializer_list<std::string>& constraints = std::initializer_list<std::string>(),
   			   const std::initializer_list<std::string>& constraintValues = std::initializer_list<std::string>());

	std::string Delete(std::string table,
			   const std::initializer_list<std::string>& collums,
			   const std::initializer_list<std::string>& values);

private:
	sql::Driver* m_Driver;
	std::unique_ptr<sql::Connection> m_Connection;
	std::string m_Schema;

	CQueryBuilder m_QueryBuilder;
};

CMySQLConnectionInterface::CMySQLConnectionInterface(const std::string& IP, const std::string& username, const std::string& pswd)
  :m_Driver(nullptr), m_Connection(nullptr), m_Schema(std::string()), m_QueryBuilder(CQueryBuilder()) 
{
	try{
		m_Driver = get_driver_instance();
		m_Connection = std::unique_ptr<sql::Connection>(m_Driver->connect(IP.c_str(), username.c_str(), pswd.c_str()));
	} catch(sql::SQLException& e){
		std::cout << "SQL exception when creating interface: " << e.what() << std::endl;
		std::cerr << "SQL exception when creating interface: " << e.what() << std::endl;
	}
}

CMySQLConnectionInterface::~CMySQLConnectionInterface() { }

void CMySQLConnectionInterface::setSchema(const std::string& schema) 
{
	try{
		m_Connection->setSchema(schema); m_Schema = schema;
	} catch(sql::SQLException& e){
		std::cout << "SQL exception when selecting a schema: " << e.what() << std::endl;
		std::cerr << "SQL exception when selecting a schema: " << e.what() << std::endl;
	}
}

std::string CMySQLConnectionInterface::Select(const std::initializer_list<std::string>& collums, 
			  		      const std::string& table, 
		      			   const std::initializer_list<std::string>& constraints, 
				   const std::initializer_list<std::string>& constraintValues, 
					   	const std::string& order, 
			   			int limit)
{
	if(constraints.size() != constraintValues.size()){
		std::cout << "SELECT: wrong parameters format during query for " << table << std::endl << "Detail: different number of constraints and their values" << std::endl;
		std::cerr << "SELECT: wrong parameters format during query for " << table << std::endl << "Detail: different number of constraints and their values" << std::endl;
		return std::string("");
	}
	
	std::string query = m_QueryBuilder.Select(collums, table, constraints, constraintValues, order, limit);
	try{
		std::unique_ptr<sql::PreparedStatement> stmt = std::unique_ptr<sql::PreparedStatement>(m_Connection->prepareStatement(query));
		int num = 1;
		for(auto& i: constraintValues){
			std::string tmp = i; tmp.erase(0,1);
			stmt->setString(num++, tmp);
		}
	
		std::unique_ptr<sql::ResultSet> result = std::unique_ptr<sql::ResultSet>(stmt->executeQuery());
		
		std::string ret;
		while(result->next()){
			for(auto &i: collums){
				ret += result->getString(i);
				ret += std::string("  ");
			}
		}
		
		return ret;
		
	} catch(sql::SQLException& e) {
		std::cout << "SELECT EXCEPTION IN QUERY FOR " << table << ": " << e.what() << std::endl;
		std::cerr << "SELECT EXCEPTION IN QUERY FOR " << table << ": " << e.what() << std::endl;
	}
	
	return std::string("");
}

std::string CMySQLConnectionInterface::Insert(std::string table,
			   const std::initializer_list<std::string>& collums,
			   const std::initializer_list<std::string>& values)
{
	if(collums.size() != values.size()){
		std::cout << "INSERT: wrong parameters format during query for " << table << std::endl << "Detail: different number of collums and their values" << std::endl;
		std::cerr << "INSERT: wrong parameters format during query for " << table << std::endl << "Detail: different number of collums and their values" << std::endl;
		return std::string("");	
	}

	std::string query = m_QueryBuilder.Insert(table, collums, values);
	try{
		std::unique_ptr<sql::PreparedStatement> stmt = std::unique_ptr<sql::PreparedStatement>(m_Connection->prepareStatement(query));
		int num = 1;
		for(auto& i: values){
			stmt->setString(num++, i);
		}

		std::unique_ptr<sql::ResultSet> result = std::unique_ptr<sql::ResultSet>(stmt->executeQuery());
	
		std::string ret;
		while(result->next()){
			for(auto &i: collums){
				ret += result->getString(i);
				ret += std::string("  ");
			}
		}
		
		return ret;
	} catch(sql::SQLException& e){
		std::cout << "INSERT EXCEPTION IN QUERY FOR " << table << ": " << e.what() << std::endl;
		std::cerr << "INSERT EXCEPTION IN QUERY FOR " << table << ": " << e.what() << std::endl;
	}
	return std::string("");
}

std::string CMySQLConnectionInterface::Update(std::string table,
			   const std::initializer_list<std::string>& collums,
			   const std::initializer_list<std::string>& values,
			   const std::initializer_list<std::string>& constraints,
   			   const std::initializer_list<std::string>& constraintValues)
{

	if(collums.size() != values.size()){
		std::cout << "UPDATE: wrong parameters format during query for " << table << std::endl << "Detail: different number of collums and their values" << std::endl;
		std::cerr << "UPDATE: wrong parameters format during query for " << table << std::endl << "Detail: different number of collums and their values" << std::endl;
		return std::string("");	
	}
	
	if(constraints.size() != constraintValues.size()){
		std::cout << "UPDATE: wrong parameters format during query for " << table << std::endl << "Detail: different number of constraints and their values" << std::endl;
		std::cerr << "UPDATE: wrong parameters format during query for " << table << std::endl << "Detail: different number of constraints and their values" << std::endl;
		return std::string("");	
	}

	std::string query = m_QueryBuilder.Update(table, collums, values, constraints, constraintValues);
	try{
		std::unique_ptr<sql::PreparedStatement> stmt = std::unique_ptr<sql::PreparedStatement>(m_Connection->prepareStatement(query));
		int num = 1;
		for(auto& i: values){
			stmt->setString(num++, i);
		}
		for(auto& i: constraintValues){
			std::string tmp = i; tmp.erase(0,1);
			stmt->setString(num++, tmp);
		}
	
		std::unique_ptr<sql::ResultSet> result = std::unique_ptr<sql::ResultSet>(stmt->executeQuery());
		std::string ret;
		while(result->next()){
			for(auto &i: collums){
				ret += result->getString(i);
				ret += std::string("  ");
			}
		}
		return ret;
	}
	catch(sql::SQLException& e){
		std::cout << "UPDATE EXCEPTION IN QUERY FOR " << table << ": " << e.what() << std::endl;
		std::cerr << "UPDATE EXCEPTION IN QUERY FOR " << table << ": " << e.what() << std::endl;
	}
	return std::string("");
}

std::string CMySQLConnectionInterface::Delete(std::string table,
		   const std::initializer_list<std::string>& collums,
		   const std::initializer_list<std::string>& values)
{
	if(collums.size() != values.size()){
		std::cout << "DELETE: wrong parameters format during query for " << table << std::endl << "Detail: different number of collums and their values" << std::endl;
		std::cerr << "DELETE: wrong parameters format during query for " << table << std::endl << "Detail: different number of collums and their values" << std::endl;
		return std::string("");	
	}


	std::string query = m_QueryBuilder.Delete(table, collums, values);
	try{
		std::unique_ptr<sql::PreparedStatement> stmt = std::unique_ptr<sql::PreparedStatement>(m_Connection->prepareStatement(query));
		int num = 1;
		for(auto& i: values){
			std::string tmp = i; tmp.erase(0,1);
			stmt->setString(num++, tmp);
		}
	
		std::unique_ptr<sql::ResultSet> result = std::unique_ptr<sql::ResultSet>(stmt->executeQuery());
		std::string ret;
		while(result->next()){
			for(auto &i: collums){
				ret += result->getString(i);
				ret += std::string("  ");
			}
		}
		return ret;
	} catch(sql::SQLException& e){
		std::cout << "DELETE EXCEPTION IN QUERY FOR " << table << ": " << e.what() << std::endl;
		std::cerr << "DELETE EXCEPTION IN QUERY FOR " << table << ": " << e.what() << std::endl;
		
	}
	return std::string("");
}

int main()
{
	CQueryBuilder q2;
	CMySQLConnectionInterface q("127.0.0.1", "admin", "passwd");	//
	q.setSchema("users");

	std::cout << q.Select({"id", "username", "password"}, "_users", {"id"}, {"=1"}) << std::endl;
	
//	std::cout << q.Insert("_users", {"id", "username"}, {"5", "kreten"}) << std::endl;
//	std::cout << q.Update("_users", {"username", "id"}, {"jebak", "69"}, {"username"}, {"=koko"}) << std::endl;
//	std::cout << q.Delete("_users", {"username"}, {"=jebak"}) << std::endl;		
}








