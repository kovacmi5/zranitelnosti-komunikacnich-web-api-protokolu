#include <iostream>
#include <memory>
#include <string>
#include <initializer_list>

class CQueryBuilder
{
public:
	CQueryBuilder();
	CQueryBuilder(const char* base);
	CQueryBuilder(const std::string& base);

	std::string Select(const std::initializer_list<std::string>& collums, 
			   const std::string& table, 
			   const std::initializer_list<std::string>& constraints = std::initializer_list<std::string>(), 
			   const std::initializer_list<std::string>& constraintValues = std::initializer_list<std::string>(), 
			   const std::string& order = "", 
			   int limit = 0);

	std::string Insert(std::string table,
			   const std::initializer_list<std::string>& collums,
			   const std::initializer_list<std::string>& values);

	std::string Update(std::string table,
			   const std::initializer_list<std::string>& collums,
			   const std::initializer_list<std::string>& values,
			   const std::initializer_list<std::string>& constraints = std::initializer_list<std::string>(),
   			   const std::initializer_list<std::string>& constraintValues = std::initializer_list<std::string>());

	std::string Delete(std::string table,
			   const std::initializer_list<std::string>& collums,
			   const std::initializer_list<std::string>& values);

	std::string getQuery();	
	void clear();
private:
	
	void append(char* toAppend);
	void append(std::string toAppend);
	template<typename T> void append(const std::initializer_list<T>& toAppend);
	template<typename T> void appendParameters(const std::initializer_list<T>& parameters, const std::initializer_list<T>& parameterValues); 
	template<typename T> void appendConstraints(const std::initializer_list<T>& constraints, const std::initializer_list<T>& constraintValues); 
	
	std::string m_Query;
};
