#include "mySQLConnectionInterface.h"

CMySQLConnectionInterface::CMySQLConnectionInterface(const std::string& IP, const std::string& username, const std::string& pswd)
  :m_Driver(nullptr), m_Connection(nullptr), m_Schema(std::string()), m_QueryBuilder(CQueryBuilder()) 
{
	try{
		m_Driver = get_driver_instance();
		m_Connection = std::unique_ptr<sql::Connection>(m_Driver->connect(IP.c_str(), username.c_str(), pswd.c_str()));
	} catch(sql::SQLException& e){
		std::cout << "SQL exception when creating interface: " << e.what() << std::endl;
		std::cerr << "SQL exception when creating interface: " << e.what() << std::endl;
	}
}

CMySQLConnectionInterface::~CMySQLConnectionInterface() { }

void CMySQLConnectionInterface::setSchema(const std::string& schema) 
{
	try{
		m_Connection->setSchema(schema); m_Schema = schema;
	} catch(sql::SQLException& e){
		std::cout << "SQL exception when selecting a schema: " << e.what() << std::endl;
		std::cerr << "SQL exception when selecting a schema: " << e.what() << std::endl;
	}
}

std::string CMySQLConnectionInterface::Select(const std::initializer_list<std::string>& collums, 
			  		      const std::string& table, 
		      			   const std::initializer_list<std::string>& constraints, 
				   const std::initializer_list<std::string>& constraintValues, 
					   	const std::string& order, 
			   			int limit)
{
	if(constraints.size() != constraintValues.size()){
		std::cout << "SELECT: wrong parameters format during query for " << table << std::endl << "Detail: different number of constraints and their values" << std::endl;
		std::cerr << "SELECT: wrong parameters format during query for " << table << std::endl << "Detail: different number of constraints and their values" << std::endl;
		return std::string("");
	}
	
	std::string query = m_QueryBuilder.Select(collums, table, constraints, constraintValues, order, limit);
	try{
		std::unique_ptr<sql::PreparedStatement> stmt = std::unique_ptr<sql::PreparedStatement>(m_Connection->prepareStatement(query));
		int num = 1;
		for(auto& i: constraintValues){
			std::string tmp = i; tmp.erase(0,1);
			stmt->setString(num++, tmp);
		}
	
		std::unique_ptr<sql::ResultSet> result = std::unique_ptr<sql::ResultSet>(stmt->executeQuery());
		
		std::string ret;
		while(result->next()){
			for(auto &i: collums){
				ret += result->getString(i);
				ret += std::string("  ");
			}
		}
		
		return ret;
		
	} catch(sql::SQLException& e) {
		std::cout << "SELECT EXCEPTION IN QUERY FOR " << table << ": " << e.what() << std::endl;
		std::cerr << "SELECT EXCEPTION IN QUERY FOR " << table << ": " << e.what() << std::endl;
	}
	
	return std::string("");
}

std::string CMySQLConnectionInterface::Insert(std::string table,
			   const std::initializer_list<std::string>& collums,
			   const std::initializer_list<std::string>& values)
{
	if(collums.size() != values.size()){
		std::cout << "INSERT: wrong parameters format during query for " << table << std::endl << "Detail: different number of collums and their values" << std::endl;
		std::cerr << "INSERT: wrong parameters format during query for " << table << std::endl << "Detail: different number of collums and their values" << std::endl;
		return std::string("");	
	}

	std::string query = m_QueryBuilder.Insert(table, collums, values);
	try{
		std::unique_ptr<sql::PreparedStatement> stmt = std::unique_ptr<sql::PreparedStatement>(m_Connection->prepareStatement(query));
		int num = 1;
		for(auto& i: values){
			stmt->setString(num++, i);
		}

		std::unique_ptr<sql::ResultSet> result = std::unique_ptr<sql::ResultSet>(stmt->executeQuery());
	
		std::string ret;
		while(result->next()){
			for(auto &i: collums){
				ret += result->getString(i);
				ret += std::string("  ");
			}
		}
		
		return ret;
	} catch(sql::SQLException& e){
		std::cout << "INSERT EXCEPTION IN QUERY FOR " << table << ": " << e.what() << std::endl;
		std::cerr << "INSERT EXCEPTION IN QUERY FOR " << table << ": " << e.what() << std::endl;
	}
	return std::string("");
}

std::string CMySQLConnectionInterface::Update(std::string table,
			   const std::initializer_list<std::string>& collums,
			   const std::initializer_list<std::string>& values,
			   const std::initializer_list<std::string>& constraints,
   			   const std::initializer_list<std::string>& constraintValues)
{

	if(collums.size() != values.size()){
		std::cout << "UPDATE: wrong parameters format during query for " << table << std::endl << "Detail: different number of collums and their values" << std::endl;
		std::cerr << "UPDATE: wrong parameters format during query for " << table << std::endl << "Detail: different number of collums and their values" << std::endl;
		return std::string("");	
	}
	
	if(constraints.size() != constraintValues.size()){
		std::cout << "UPDATE: wrong parameters format during query for " << table << std::endl << "Detail: different number of constraints and their values" << std::endl;
		std::cerr << "UPDATE: wrong parameters format during query for " << table << std::endl << "Detail: different number of constraints and their values" << std::endl;
		return std::string("");	
	}

	std::string query = m_QueryBuilder.Update(table, collums, values, constraints, constraintValues);
	try{
		std::unique_ptr<sql::PreparedStatement> stmt = std::unique_ptr<sql::PreparedStatement>(m_Connection->prepareStatement(query));
		int num = 1;
		for(auto& i: values){
			stmt->setString(num++, i);
		}
		for(auto& i: constraintValues){
			std::string tmp = i; tmp.erase(0,1);
			stmt->setString(num++, tmp);
		}
	
		std::unique_ptr<sql::ResultSet> result = std::unique_ptr<sql::ResultSet>(stmt->executeQuery());
		std::string ret;
		while(result->next()){
			for(auto &i: collums){
				ret += result->getString(i);
				ret += std::string("  ");
			}
		}
		return ret;
	}
	catch(sql::SQLException& e){
		std::cout << "UPDATE EXCEPTION IN QUERY FOR " << table << ": " << e.what() << std::endl;
		std::cerr << "UPDATE EXCEPTION IN QUERY FOR " << table << ": " << e.what() << std::endl;
	}
	return std::string("");
}

std::string CMySQLConnectionInterface::Delete(std::string table,
		   const std::initializer_list<std::string>& collums,
		   const std::initializer_list<std::string>& values)
{
	if(collums.size() != values.size()){
		std::cout << "DELETE: wrong parameters format during query for " << table << std::endl << "Detail: different number of collums and their values" << std::endl;
		std::cerr << "DELETE: wrong parameters format during query for " << table << std::endl << "Detail: different number of collums and their values" << std::endl;
		return std::string("");	
	}


	std::string query = m_QueryBuilder.Delete(table, collums, values);
	try{
		std::unique_ptr<sql::PreparedStatement> stmt = std::unique_ptr<sql::PreparedStatement>(m_Connection->prepareStatement(query));
		int num = 1;
		for(auto& i: values){
			std::string tmp = i; tmp.erase(0,1);
			stmt->setString(num++, tmp);
		}
	
		std::unique_ptr<sql::ResultSet> result = std::unique_ptr<sql::ResultSet>(stmt->executeQuery());
		std::string ret;
		while(result->next()){
			for(auto &i: collums){
				ret += result->getString(i);
				ret += std::string("  ");
			}
		}
		return ret;
	} catch(sql::SQLException& e){
		std::cout << "DELETE EXCEPTION IN QUERY FOR " << table << ": " << e.what() << std::endl;
		std::cerr << "DELETE EXCEPTION IN QUERY FOR " << table << ": " << e.what() << std::endl;
		
	}
	return std::string("");
}

