#include <iostream>
#include <memory>
#include <string>
#include <initializer_list>

#include <mysql_driver.h>
#include <mysql_connection.h>
#include <cppconn/connection.h>
#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/prepared_statement.h>

#include "queryBuilder.h"

class CMySQLConnectionInterface
{
public:
	CMySQLConnectionInterface(const std::string& IP, const std::string& username, const std::string& pswd);
	~CMySQLConnectionInterface();
	void setSchema(const std::string& schema);
	
	std::string Select(const std::initializer_list<std::string>& collums, 
			   const std::string& table, 
			   const std::initializer_list<std::string>& constraints = std::initializer_list<std::string>(), 
			   const std::initializer_list<std::string>& constraintValues = std::initializer_list<std::string>(), 
			   const std::string& order = "", 
			   int limit = 0);

	std::string Insert(std::string table,
			   const std::initializer_list<std::string>& collums,
			   const std::initializer_list<std::string>& values);

	std::string Update(std::string table,
			   const std::initializer_list<std::string>& collums,
			   const std::initializer_list<std::string>& values,
			   const std::initializer_list<std::string>& constraints = std::initializer_list<std::string>(),
   			   const std::initializer_list<std::string>& constraintValues = std::initializer_list<std::string>());

	std::string Delete(std::string table,
			   const std::initializer_list<std::string>& collums,
			   const std::initializer_list<std::string>& values);

private:
	sql::Driver* m_Driver;
	std::unique_ptr<sql::Connection> m_Connection;
	std::string m_Schema;

	CQueryBuilder m_QueryBuilder;
};
