const { gql } = require("apollo-server");

const typeDefs = gql`
type Book {
  id: Int!
  Title: String!
  Author: [Author]
}

type Author {
  id: Int!
  Name: String
  Books: [Book]
}

type User {
  id: Int!
  Username: String!
  Password: String!
  Email: String!
  Role: String!
}

type updateResponse {
  Details: String!
}

type loginResponse {
  Details: String!
}

type Query {
  books: [Book]
  authors: [Author]
  users: [User]
  book(id: Int!): Book
  author(id: Int!): Author
  user(id: Int!): User
}


type Mutation {
  login(Username: String!, Password: String!): loginResponse!
  addBook(id: ID!, title: String!): updateResponse!
  addAuthor(name: String!): updateResponse!
  addUser(username: String!, password: String!, email: String!, role: String, verification: String!): UpdateResponse!
}
`;

module.exports = typeDefs;

