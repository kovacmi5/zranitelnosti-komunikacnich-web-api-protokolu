const books = require("./data/books.js");
const authors = require("./data/authors.js");
const users = require("./data/users.js");

const resolvers = {
  Query: {
  
    books(parent, args, context, info){
      return books;
    },
    
    authors(parent, args, context, info){
      return authors;
    },
    
    users(parent, args, context, info){
      return users;
    },
    
    book(parent, args, context, info){
      return books.find((book) => book.id === args.id)
    },
    
    author(parent, args, context, info){
      return authors.find((author) => author.id === args.id)
    },
    
    user(parent, args, context, info){
      return users.find((user) => user.id === args.id)
    },	
  },
  Author: {
    Books: (Author) => {
      return [books.find((book) => book.Author === Author.Name)]
    }
  },
  Book: {
    Author: (Book) => {
      return [authors.find((author) => author.Name === Book.Author)]
    },
  },
  
  Mutation: {
    login: async (_, {Username, Password}) => {
        const user = users.find((u) => u.Username === Username);
        if(! user) return { Details: "Invalid user"};
        if(user.Password !== Password) return {Details: "Invalid password"};
        
        user.LoggedOn = 1;
                      
        return {Details: "Login succesfull"};  
    },
    addBook: async(_, {id, Title}) => {
      const newBook = {id: id, Title: Title};
      books.push(newBook);
      return updateResponse = { Details: "Update succesfull" };
    },
    addAuthor: async(_, {id, Name}) => {
      const newAuthor = {id: id, Name: Name};
      authors.push(newAuthor);
      return updateResponse = { Details: "Update succesfull" };
    },
    addUser: async(_, {id, Username, Password, Email, verification}) => {
      const who = users.find((u) => u.Username === verification)
      if(!who) return updateResponse = { Details: "Invalid username" };
      if(who.LoggedOn !== 1) return updateResponse = { Details: "Not logged on" };
      
      const newUser = {
        id: id, Username: Username, Password: Password,
        Email: Email, Role: "User"};
      users.push(newUser);
      return updateResponse = { Details: "Update succesfull" };
    },
  },
};

module.exports = resolvers;
