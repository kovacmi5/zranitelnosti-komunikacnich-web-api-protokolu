const { ApolloServer } = require("apollo-server");

const typeDefs = require("./schema");
const resolvers = require("./resolvers");

const books = require("./data/books.js");
const authors = require("./data/authors.js");
const users = require("./data/users.js");

const server = new ApolloServer({ 
  typeDefs,
  resolvers,
});

server.listen().then(() => {
  console.log("server is running");
});
