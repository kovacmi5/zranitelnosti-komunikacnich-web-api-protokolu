const { ApolloServer } = require("apollo-server");
const { gql } = require("apollo-server");
const { GraphQLError } = require("graphql")
const jwt = require("jsonwebtoken");
const CryptoJS = require("crypto-js");
const {shield, rule, and, inputRule} = require("graphql-shield");
const { makeExecutableSchema } = require("@graphql-tools/schema");
const { applyMiddleware } = require("graphql-middleware");
const fs = require("fs");

const books = require("./data/books")
const authors = require("./data/authors")
const users = require("./data/users")
const registeringUsers = require("./data/registeringUsers")
const loggedUsers = require("./data/loggedUsers")
const serverKey = require("./server.key");
//const serverKey = "testPassword1234";			WORKS, GOOD FOR FAST TESTING!
//const invertedQueryMap = require("./invertedQueryMap");	FOR WHEN YOU ALREADY HAVE ONE


const typeDefs = gql` 
type Book {
  Title: String!
  Author: [Author]
  Permissions: [Permission]
}

type Author {
  Name: String
  Books: [Book]
  Permissions: [Permission]
}

type User {
  Username: String!
  Password: String!
  Email: String!
  Role: String!
}

type updateResponse {
  Details: String!
}

type Query {
  books: [Book]
  authors: [Author]
  users: [User]
  book(id: Int!): Book
  author(id: Int!): Author
  user(id: Int!): User
}

type Mutation {
  addUser(username: String!, password: String!, email: String!, role: String, verification: String) : loginResponse!

  makeAdmin(username: String!): updateResponse!
  addBook(id: ID!, title: String!): updateResponse!
  addAuthor(name: String!): updateResponse!
  
  register(username: String!, password: String!, email: String!): registerResponse!
  verify(verificationString: String!): verifyResponse!
  login(username: String!, password: String!): loginResponse!
  logout(username: String!, password: String!): logoutResponse!
  changePassword(username: String!, oldPassword: String!, newPassword: String!): changePasswordResponse!
}

type registerResponse {Details: String!}
type verifyResponse {Details: String!}
type loginResponse {Details: String!}
type logoutResponse {Details: String!}
type changePasswordResponse {Details: String!}

type Permission {
        Who: String!
        What: String!
}
`;

const resolvers = {
  Query: {  
    books(parent, args, context, info){ return books; },
    authors(parent, args, context, info){ return authors;},
    users(parent, args, context, info){ return users;},
    
    book(parent, args, context, info){
        return books.find((book) => book.id === args.id)
    },
    
    author(parent, args, context, info){
      return authors.find((author) => author.id === args.id)
    },
    
    user(parent, args, context, info){
      return users.find((user) => user.id === args.id)
    },
  },
  Author: {
    Books: (Author) => {
      return [books.find((book) => book.Author === Author.Name)]
    }
  },
  Book: {
    Author: (Book) => {
      return [authors.find((author) => author.Name === Book.Author)]
    },
  },
  
  Mutation: {
    addBook: async(_, {Title, Author}) => {
      books.push({Title: Title, Author: Author});
      return updateResponse = { Details: "Update succesfull" };
    },
    addAuthor: async(_, {Name}) => {
      authors.push({Name: Name});
      return updateResponse = { Details: "Update succesfull" };
    },
    makeAdmin: async(_, {Username}) => {
      const user = users.find((u) => u.Username === Username);
      if(!user) return {Details: "Update failed"};

      user.Role = "admin";
      return {Details: "Update successfull"};
    },
    register: async(_, {username, password, email}) => {
      const anotherUser = users.find((u) => u.Username === username);
      if(anotherUser) return registerResponse = {Details: "User already exists"};
      
      if(8 > password.length) return registerResponse = {Details: "Password does not meet complexity requirements: must be at least 8 chars long"};
      let pwdGoodSigns = 0;
      if(password.match(/[a-z]/)) pwdGoodSigns++;
      if(password.match(/[A-Z]/)) pwdGoodSigns++;
      if(password.match(/[0-9]/)) pwdGoodSigns++;
      if(password.match(/[<>!@#$%^&*?]/gi)) pwdGoodSigns++;
      
      if(3 > pwdGoodSigns) return registerResponse = {Details: "Password must contain at least 3 of these character signs: small letter, capital letter, number, signs: <>!@#$%^&*?"};
      
      const random = (Math.random() + 1).toString(36).substring(7);
      console.log("random: " + random);
      registeringUsers.push({
      	Username: username, 
      	Password: CryptoJS.SHA256(CryptoJS.SHA256(password).toString()).toString(), 
      	Email: email, 
      	RegisteringSecret: random});
      

	return {Details: CryptoJS.AES.encrypt(
		random + username, serverKey
	).toString()};
    },
    verify: async(_, {verificationString}) => {
  
  	const detailsHex = CryptoJS.AES.decrypt(verificationString, serverKey).toString();
    
	var details = "";
	for(var i = 0; i < detailsHex.length; i+=2)
		details += String.fromCharCode(parseInt(detailsHex.substr(i,2), 16));    
	console.log(details);
	console.log(details.substring(5));
	console.log(details.substring(0,5));
	
	console.log(registeringUsers);
	//depending of length of the random string
	const registeringUser = registeringUsers.find(
		(u)=> u.Username === details.substring(5) && 
		u.RegisteringSecret === details.substring(0,5));
	
	if(! registeringUser) return {Details: "Verification went wrong"};
    	
	users.push({
		Username: registeringUser.Username, 
		Password: registeringUser.Password, 
		Email: registeringUser.Email, 
		Role: "User"
	});
	
	return { Details: "Registering succesfull" };
  },
  login: async (_, {username, password}) => {
  	const user = users.find((u) => u.Username === username);
  	if(!user) return {Details: "Invalid credentials"};
  	console.log("here");
  	
  	console.log(user.Password);
    	console.log(CryptoJS.SHA256(password).toString());
  	if(user.Password !== CryptoJS.SHA256(password).toString()) return {Details: "Invalid credentials"};
  	
  	const random = (Math.random() + 1).toString(36).substring(7);
  	
  	const token = jwt.sign({
  		Username: user.Username, Role: user.Role, Random: random}, serverKey);
  	
  	loggedUsers.push({Username: user.Username, Random: random});
  		
  	return {Details: token};
  },
  logout: async(_, {username}) => {
	const user = loggedUsers.find((u) => u.Username === username);
	loggedUsers.splice(loggedUsers.indexOf(user), 1);
  },
  changePassword: async(_, {username, oldPassword, newPassword}, ctx) => {
  	if(username !== ctx.Username) return {Details: "Change password failed"};
  
  	const user = users.find((u) => u.Username === username);
  	if(!user) return {Details: "This should never happen. Also, i owe you a flask if you read this"};
  	
  	if(user.Password !== CryptoJS.SHA256(oldPassword).toString()) {Details: "Change password failed"}
  	
      let pwdGoodSigns = 0;
      if(newPassword.match(/[a-z]/)) pwdGoodSigns++;
      if(newPassword.match(/[A-Z]/)) pwdGoodSigns++;
      if(newPassword.match(/[0-9]/)) pwdGoodSigns++;
      if(newPassword.match(/[<>!@#$%^&*?]/gi)) pwdGoodSigns++;
      
      if(3 > pwdGoodSigns) return {Details: "Password must contain at least 3 of these character signs: small letter, capital letter, number, signs: <>!@#$%^&*?"};
      
      user.Password = CryptoJS.SHA256(CryptoJS.SHA256(newPassword).toString()).toString();
      
      return {Details: "Success"}
  },
  addUser: async (_, {username, password, email, verification}) => {
  	return true;    //left here for testing csrf - will never be called
  }
}
};

function hasReadPermissions(object, username){
    const user = users.find((u) => u.Username === username);
    if(!user) return false;

    const permissions = object.Permissions;
    if(!permissions) return true;

    for(let i = 0; i < permissions.length; ++i){
        if(permissions[i].Who === username){
            if(permissions[i].What === "owner" || permissions[i].What === "read")
                return true;
        }
    }
    return false;
}

function hasWritePermissions(object, username){
    const user = users.find((u) => u.Username === username);
    if(!user) return false;

    const permissions = object.Permissions;
    if(!permissions) return true;

    for(let i = 0; i < permissions.length; ++i){
        if(permissions[i].Who === username){
            if(permissions[i].What === "owner" || permissions[i].What === "write")
                return true;
        }
    }
    return false;
}

const isAuthenticated = rule()(async (parent, args, ctx, info) =>{

  console.log(ctx.Username);
  const user = loggedUsers.find((u) => u.Username === ctx.Username && ctx.Random === u.Random);
  if(user) return true;
  
  return false; 
});

///or any other role...
const isAdmin = rule()(async (parent, args, ctx, info) =>{
  const user = loggedUsers.find((u) => u.Username === ctx.Username);
  if(!user) return false;
  if(user.Role === 'Admin') return true;
  
  return false;
});

const allow = rule()(async (parent, args, ctx, info) =>{
	return true;
});


const permissions = shield({
	Query: {
	  users: isAuthenticated,
	  books: isAuthenticated,
    	  authors: isAuthenticated,
    	  user: isAuthenticated,
	  book: isAuthenticated,
    	  author: isAuthenticated,
	},
	Mutation: {
	  makeAdmin: isAdmin,
	  logout: isAuthenticated,
	  register: allow,
	  changePassword: isAuthenticated,
	  addUser: isAdmin,
	},
});

const server = new ApolloServer({ 
	schema: applyMiddleware(makeExecutableSchema({typeDefs, resolvers}), permissions),
	context: async ({req}) => {
		var carryOn = true;		//false means no jwt is carried on and all functions fail authentization
		if(process.env.NODE_ENV === 'production'){
						
			if(!req) carryOn = false;
	
			if(req.body && request.body.id){
				const query = invertedQueryMap[req.body.id];

				if(query) req.body.query = query;				
				else carryOn = false;
			}
			else carryOn = false;

		} else {
			if("IntrospectionQuery" !== req.body.operationName )
				{ fs.appendFileSync("queries.gql", req.body.query.toString()), fs.appendFileSync("queries.gql", ","); };
		}
		if(false === carryOn) return ""
        	let token;
		try{
			token = jwt.verify(req.headers.authorization, serverKey);
        	} catch (e) {return null;}

		return token;
	},
	formatError: (err) => {
		console.log(err);
		if("GRAPHQL_PARSE_FAILED" === err.extensions.code)
			return "400 Bad Request";
			
		if("GRAPHQL_VALIDATION_FAILED" === err.extensions.code)
			return "404 Not Found";
		
		if("BAD_USER_INPUT" === err.extensions.code)
			return "404 Not Found";
		
		if("UNAUTHENTICATED" === err.extensions.code)
			return "401 Unauthorized";
			
		if("FORBIDDEN" === err.extensions.code)
			return "403 Forbidden";
			
		if("PERSISTENT_QUERY_NOT_FOUND" === err.extensions.code)
			return "404 Not Found";
			
		if("PERSISTENT_QUERY_NOT_SUPPORTED" === err.extensions.code)
			return "503 Service Unavailable";
			
		if("INTERNAL_SERVER_ERROR" === err.extensions.code)
			return "500 Service Unavailable";
		
		return err;
   	}

});

server.listen().then(() => {
  console.log("server is running");
});

