const jwt = require("jsonwebtoken");
const CryptoJS = require("crypto-js");

const books = require("./data/books.js");
const authors = require("./data/authors.js");
const users = require("./data/users.js");
const registeringUsers = require("./data/registeringUsers.js");

const serverKey = require("server.key");

const AuthenticationResolvers = {
Mutation: {
    register: async(_, {username, password, email}) => {
      const anotherUser = users.find((u) => u.Username === username);
      if(anotherUser) return registerResponse = {Details: "User already exists"};
      
      if(8 > password.length) return registerResponse = {Details: "Password does not meet complexity requirements: must be at least 8 chars long"};
      let pwdGoodSigns = 0;
      if(password.match(/[a-z]/)) pwdGoodSigns++;
      if(password.match(/[A-Z]/)) pwdGoodSigns++;
      if(password.match(/[0-9]/)) pwdGoodSigns++;
      if(password.match(/[<>!@#$%^&*?]/gi)) pwdGoodSigns++;
      
      if(3 > pwdGoodSigns) return registerResponse = {Details: "Password must contain at least 3 of these character signs: small letter, capital letter, number, signs: <>!@#$%^&*?"};
      
      const random = (Math.random() + 1).toString(36).substring(7);
      console.log("random: " + random);
      registeringUsers.push({
      	Username: username, 
      	Password: CryptoJS.SHA256(CryptoJS.SHA256(password).toString()).toString(), 
      	Email: email, 
      	RegisteringSecret: random});
      

	return {Details: CryptoJS.AES.encrypt(
		random + username, serverKey
	).toString()};
    },
    verify: async(_, {verificationString}) => {
  
  	const detailsHex = CryptoJS.AES.decrypt(verificationString, serverKey).toString();
    
	var details = "";
	for(var i = 0; i < detailsHex.length; i+=2)
		details += String.fromCharCode(parseInt(detailsHex.substr(i,2), 16));    
	console.log(details);
	console.log(details.substring(5));
	console.log(details.substring(0,5));
	
	console.log(registeringUsers);
	//depending of length of the random string
	const registeringUser = registeringUsers.find(
		(u)=> u.Username === details.substring(5) && 
		u.RegisteringSecret === details.substring(0,5));
	
	if(! registeringUser) return {Details: "Verification went wrong"};
    	
	users.push({
		Username: registeringUser.Username, 
		Password: registeringUser.Password, 
		Email: registeringUser.Email, 
		Role: "User"
	});
	
	return { Details: "Registering succesfull" };
  },
  login: async (_, {username, password}) => {
  	const user = users.find((u) => u.Username === username);
  	if(!user) return {Details: "Invalid credentials"};
  	console.log("here");
  	
  	console.log(user.Password);
    	console.log(CryptoJS.SHA256(password).toString());
  	if(user.Password !== CryptoJS.SHA256(password).toString()) return {Details: "Invalid credentials"};
  	
  	const random = (Math.random() + 1).toString(36).substring(7);
  	
  	const token = jwt.sign({
  		Username: user.Username, Role: user.Role, Random: random}, serverKey);
  	
  	loggedUsers.push({Username: user.Username, Random: random});
  		
  	return {Details: token};
  },
  logout: async(_, {username}) => {
	const user = loggedUsers.find((u) => u.Username === username);
	loggedUsers.splice(loggedUsers.indexOf(user), 1);
  },
  changePassword: async(_, {username, oldPassword, newPassword}, ctx) => {
  	if(username !== ctx.Username) return {Details: "Change password failed"};
  
  	const user = users.find((u) => u.Username === username);
  	if(!user) return {Details: "This should never happen. Also, i owe you a flask if you read this"};
  	
  	if(user.Password !== CryptoJS.SHA256(oldPassword).toString()) {Details: "Change password failed"}
  	
      let pwdGoodSigns = 0;
      if(newPassword.match(/[a-z]/)) pwdGoodSigns++;
      if(newPassword.match(/[A-Z]/)) pwdGoodSigns++;
      if(newPassword.match(/[0-9]/)) pwdGoodSigns++;
      if(newPassword.match(/[<>!@#$%^&*?]/gi)) pwdGoodSigns++;
      
      if(3 > pwdGoodSigns) return {Details: "Password must contain at least 3 of these character signs: small letter, capital letter, number, signs: <>!@#$%^&*?"};
      
      user.Password = CryptoJS.SHA256(CryptoJS.SHA256(newPassword).toString()).toString();
      
      return {Details: "Success"}
  },
}

module.exports = AuthenticationResolvers;

}
