function hasReadPermissions(object, username){
    const user = users.find((u) => u.Username === username);
    if(!user) return false;

    const permissions = object.Permissions;
    if(!permissions) return true;

    for(let i = 0; i < permissions.length; ++i){
        if(permissions[i].Who === username){
            if(permissions[i].What === "owner" || permissions[i].What === "read")
                return true;
        }
    }
    return false;
}

function hasWritePermissions(object, username){
    const user = users.find((u) => u.Username === username);
    if(!user) return false;

    const permissions = object.Permissions;
    if(!permissions) return true;

    for(let i = 0; i < permissions.length; ++i){
        if(permissions[i].Who === username){
            if(permissions[i].What === "owner" || permissions[i].What === "write")
                return true;
        }
    }
    return false;
}
