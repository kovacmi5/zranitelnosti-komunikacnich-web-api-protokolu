

context: async ({req}) => {
		
        let token;
		try{
			token = jwt.verify(req.headers.authorization, serverKey);
        	} catch (e) {return null;}

		return token;
	},
