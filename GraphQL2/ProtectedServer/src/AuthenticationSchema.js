const gql = require("apollo-server");

AuthenticationTypeDefs = gql`
type Mutation {
  register(username: String!, password: String!, email: String!): registerResponse!
  verify(verificationString: String!): verifyResponse!
  login(username: String!, password: String!): loginResponse!
  logout(username: String!): logoutResponse!
}

type registerResponse {Details: String!}
type verifyResponse {Details: String!}
type loginResponse {Details: String!}
type logoutResponse {Details: String!}
`;

module.exports = AuthenticationTypeDefs;
