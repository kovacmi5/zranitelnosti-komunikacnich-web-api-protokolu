const { gql } = require("apollo-server");
 
const typeDefs = gql` 
type Book {
  Title: String!
  Author: [Author]
  Permissions: [Permission]
}

type Author {
  Name: String
  Books: [Book]
  Permissions: [Permission]
}

type User {
  Username: String!
  Password: String!
  Email: String!
  Role: String!
}

type updateResponse {
  Details: String!
}

type Query {
  books: [Book]
  authors: [Author]
  users: [User]
  book(id: Int!): Book
  author(id: Int!): Author
  user(id: Int!): User
}

type Mutation {
  addUser(username: String!, password: String!, email: String!, role: String, verification: String) : loginResponse!

  makeAdmin(username: String!): updateResponse!
  addBook(id: ID!, title: String!): updateResponse!
  addAuthor(name: String!): updateResponse!
}
`;

module.exports = typeDefs;
