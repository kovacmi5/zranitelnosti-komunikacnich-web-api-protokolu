formatError: (err) => {
		console.log(err);
		if("GRAPHQL_PARSE_FAILED" === err.extensions.code)
			return "400 Bad Request";
			
		if("GRAPHQL_VALIDATION_FAILED" === err.extensions.code)
			return "404 Not Found";
		
		if("BAD_USER_INPUT" === err.extensions.code)
			return "404 Not Found";
		
		if("UNAUTHENTICATED" === err.extensions.code)
			return "401 Unauthorized";
			
		if("FORBIDDEN" === err.extensions.code)
			return "403 Forbidden";
			
		if("PERSISTENT_QUERY_NOT_FOUND" === err.extensions.code)
			return "404 Not Found";
			
		if("PERSISTENT_QUERY_NOT_SUPPORTED" === err.extensions.code)
			return "503 Service Unavailable";
			
		if("INTERNAL_SERVER_ERROR" === err.extensions.code)
			return "500 Service Unavailable";
		
		return err;
   	}
