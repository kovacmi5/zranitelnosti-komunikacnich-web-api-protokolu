
const server = new ApolloServer({ 
	schema: applyMiddleware(makeExecutableSchema({typeDefs, resolvers}), permissions),
	context: async ({req}) => {
		var carryOn = true;		//false means no jwt is carried on and all functions fail authentization
		if(process.env.NODE_ENV === 'production'){
						
			if(!req) carryOn = false;
	
			if(req.body && request.body.id){
				const query = invertedQueryMap[req.body.id];

				if(query) req.body.query = query;				
				else carryOn = false;
			}
			else carryOn = false;

		} else {
			if("IntrospectionQuery" !== req.body.operationName )
				{ fs.appendFileSync("queries.gql", req.body.query.toString()), fs.appendFileSync("queries.gql", ","); };
		}
		if(false === carryOn) return ""
        	let token;
		try{
			token = jwt.verify(req.headers.authorization, serverKey);
        	} catch (e) {return null;}

		return token;
	},

});
