const books = [
{id: 1, Title: 'The Awakening', Author: 'Kate Chopin',},
{id: 2, Title: 'City of Glass', Author: 'Paul Auster',},
];

module.exports = books;
