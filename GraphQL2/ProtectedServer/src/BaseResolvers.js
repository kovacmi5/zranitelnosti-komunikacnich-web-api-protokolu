const books = require("./data/books.js");
const authors = require("./data/authors.js");
const users = require("./data/users.js");

const resolvers = {
  Query: {  
    books(parent, args, context, info){ return books; },
    authors(parent, args, context, info){ return authors;},
    users(parent, args, context, info){ return users;},
    
    book(parent, args, context, info){
        return books.find((book) => book.id === args.id)
    },
    
    author(parent, args, context, info){
      return authors.find((author) => author.id === args.id)
    },
    
    user(parent, args, context, info){
      return users.find((user) => user.id === args.id)
    },
  },
  Author: {
    Books: (Author) => {
      return [books.find((book) => book.Author === Author.Name)]
    }
  },
  Book: {
    Author: (Book) => {
      return [authors.find((author) => author.Name === Book.Author)]
    },
  },
  
  Mutation: {
    addBook: async(_, {Title, Author}) => {
      books.push({Title: Title, Author: Author});
      return updateResponse = { Details: "Update succesfull" };
    },
    addAuthor: async(_, {Name}) => {
      authors.push({Name: Name});
      return updateResponse = { Details: "Update succesfull" };
    },
    makeAdmin: async(_, {Username}) => {
      const user = users.find((u) => u.Username === Username);
      if(!user) return {Details: "Update failed"};

      user.Role = "admin";
      return {Details: "Update successfull"};
    }
  }
};

module.exports = BaseResolvers;
