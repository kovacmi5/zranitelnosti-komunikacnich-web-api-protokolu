const gql = require("apollo-server");

const AutorizationTypeDefs = gql`
    type Permission {
        Who: String!
        What: String!
    }
`;

module.export = AutorizationTypeDefs;
