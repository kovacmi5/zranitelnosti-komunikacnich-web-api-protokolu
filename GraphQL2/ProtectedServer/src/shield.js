const {shield, rule, and, inputRule} = require("graphql-shield");

const isAuthenticated = rule()(async (parent, args, ctx, info) =>{
  const user = loggedUsers.find((u) => u.Username === ctx.Username);
  if(user) return true;
  
  return false; 
});

///or any other role...
const isAdmin = rule()(async (parent, args, ctx, info) =>{
  const user = loggedUsers.find((u) => u.Username === ctx.Username);
  if(!user) return false;
  if(user.Role === 'Admin') return true;
  
  return false;
});

const permissions = shield({
	Query: {
	  users: isAuthenticated,
	  books: isAuthenticated,
    authors: isAuthenticated,
    user: isAuthenticated,
	  book: isAuthenticated,
    author: isAuthenticated,
    logoff: isAuthenticated,
	},
	Mutation: {
	  addUser: isAdmin //and(isAuthenticated, isAdmin)
	}
});

module.exports = { permissions };
