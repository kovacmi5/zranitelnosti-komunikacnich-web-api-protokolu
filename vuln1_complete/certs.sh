#!/bin/bash

Password=SecurePassword

# Generate valid CA
openssl genrsa -passout pass:$Password -aes256 -out ca.key 4096
openssl req -passin pass:$Password -new -x509 -days 365 -key ca.key -out ca.crt -subj  "/C=SP/ST=Spain/L=Valdepenias/O=Test/OU=Test/CN=Root CA"

# Generate valid Server Key/Cert
openssl genrsa -passout pass:$Password -aes256 -out server.key 4096
openssl req -passin pass:$Password -new -key server.key -out server.csr -subj  "/C=SP/ST=Spain/L=Valdepenias/O=Test/OU=Server/CN=localhost"
openssl x509 -req -passin pass:$Password -days 365 -in server.csr -CA ca.crt -CAkey ca.key -set_serial 01 -out server.crt

# Remove passphrase from the Server Key
openssl rsa -passin pass:$Password -in server.key -out server.key

# Generate valid Client Key/Cert
openssl genrsa -passout pass:$Password -aes256 -out client.key 4096
openssl req -passin pass:$Password -new -key client.key -out client.csr -subj  "/C=SP/ST=Spain/L=Valdepenias/O=Test/OU=Client/CN=localhost"
openssl x509 -passin pass:$Password -req -days 365 -in client.csr -CA ca.crt -CAkey ca.key -set_serial 01 -out client.crt

# Remove passphrase from Client Key
openssl rsa -passin pass:$Password -in client.key -out client.key
