#include <grpc/grpc.h>
#include <grpcpp/channel.h>
#include <grpcpp/client_context.h>
#include <grpcpp/create_channel.h>
#include <grpcpp/security/credentials.h>
#include <google/protobuf/descriptor.h>



#include "thesis.grpc.pb.h"


#include <fstream>
#include <iostream>
#include <string>
#include <cstdlib>

using grpc::Channel;
using grpc::ClientContext;
using grpc::ClientWriter;
using grpc::ClientReader;
using grpc::ServerReaderWriter;
using grpc::Status;
using google::protobuf::Descriptor;
using google::protobuf::FieldDescriptor;
using google::protobuf::Reflection;

using thesis::vulnerabilitiesService;
using thesis::basicMessage;
using thesis::errDisclosureMessage;
using thesis::errDisclosureReply;
using thesis::sqlInjectionMessage;
using thesis::sqlInjectionReply;

void testUnsecuredCredentials(std::unique_ptr<vulnerabilitiesService::Stub> stub)
{
	ClientContext context;
	basicMessage msg, rpl;

	msg.set_text("If you can read this, something went wrong");
	
	Status status = stub->unsecuredCredentials(&context, msg, &rpl);
}

void testInfoDisclosure(std::unique_ptr<vulnerabilitiesService::Stub> stub)
{
	ClientContext context;
	errDisclosureMessage msg;
	errDisclosureReply rpl;

	msg.set_id(1);	

	Status status = stub->protobufErrorDisclosure(&context, msg, &rpl);
	
	const Descriptor* desc = rpl.GetDescriptor();
	const Reflection* refl = rpl.GetReflection();
	
	std::cout << "Querying server for all data for user with id number 1 (i hope it's an admin!)" << std::endl << std::endl;
	
	std::cout << "Iterating through every single message field:" << std::endl;
	for(int i = 0; i < desc->field_count(); ++i){
		const FieldDescriptor* field = desc->field(i);
		std::cout << "Field name: " << field->name();
		std::cout << ", field data type: " << field->type_name();
		
		if(!field->is_repeated()){
			std::cout << ", field value: ";
			switch(field->type()){
			  case FieldDescriptor::TYPE_STRING: std::cout << refl->GetString(rpl, field); break;
			  case FieldDescriptor::TYPE_INT32:  std::cout << refl->GetInt32(rpl, field);  break;
			}

		}
		else {
			std::cout << "is repeated";
		}
		std::cout << std::endl;
	}
}

void testSQLinjection(std::unique_ptr<vulnerabilitiesService::Stub> stub)
{
	ClientContext context;
	sqlInjectionMessage msg;
	sqlInjectionReply rpl;
	
	msg.set_username("'Administrator' OR 1=1");
	std::unique_ptr<ClientReader<sqlInjectionReply>> reader(stub->sqlInjection(&context, msg));
	
	while(reader->Read(&rpl)){
		std::cout << "username: " << rpl.username() << std::endl;
		std::cout << "id: " << rpl.id() << std::endl;
	}
}

std::string readKeycert(const char* filename)
{
	std::ifstream file(filename);
	std::string ret((std::istreambuf_iterator<char>(file)),
			 std::istreambuf_iterator<char>());
	
//	std::getline(file, ret);
	file.close();
	
	std::cout << std::endl << ret << std::endl;
	
	return ret;
}

int main(int argc, char** argv)
{
	if(2 > argc){
		std::cout << "Usage: ./client <vulnerability number>" << std::endl;
		std::cout << "1: communication via unsecured credentials" << std::endl;
		std::cout << "2: protobuf information disclosure" << std::endl;
		std::cout << "3: sql injection" << std::endl;
	}

	std::string cacert = readKeycert("server.crt");
	
	grpc::SslCredentialsOptions sslOptions;
	sslOptions.pem_root_certs = cacert;
	
	//auto sslCreds = grpc::SslCredentials(sslOptions);
	

	//constexpr char kCaCertPath[] = "ca.crt";
	//grpc::SslCredentialsOptions sslOptions;
	//sslOptions.pem_root_certs = ReadFile(kCaCertPath);
	//grpc::ChannelArguments args;
	//args.SetString(GRPC_SSL_TARGET_NAME_OVERRIDE_ARG, "localhost:50051");
	
	

	std::unique_ptr<vulnerabilitiesService::Stub> stub = vulnerabilitiesService::NewStub(grpc::CreateChannel("localhost:50051", grpc::SslCredentials(sslOptions)));
	int n = atoi(argv[1]);
	switch(n){
	  case 1: testUnsecuredCredentials(std::move(stub)); break;
	  case 2: testInfoDisclosure(std::move(stub)); break;
	  case 3: testSQLinjection(std::move(stub)); break;
	  default: std::cout << "Wrong parameter, exiting" << std::endl;
	}
	
	return 0;			
}
