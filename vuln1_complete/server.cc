#include <grpc/grpc.h>
#include <grpcpp/server.h>
#include <grpcpp/server_builder.h>
#include <grpcpp/server_context.h>
#include <grpcpp/security/server_credentials.h>

#include "thesis.grpc.pb.h"

#include <mysql_driver.h>
#include <mysql_connection.h>
#include <cppconn/connection.h>
#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/prepared_statement.h>

#include <string>
#include <array>
#include <stdexcept>
#include <memory>
#include <sstream>
#include <fstream>

using grpc::Server;
using grpc::ServerBuilder;
using grpc::ServerContext;
using grpc::ServerReader;
using grpc::ServerWriter;
using grpc::ServerReaderWriter;
using grpc::Status;
using google::protobuf::Descriptor;
using google::protobuf::FieldDescriptor;
using google::protobuf::Reflection;

using thesis::vulnerabilitiesService;
using thesis::basicMessage;
using thesis::errDisclosureMessage;
using thesis::errDisclosureReply;
using thesis::sqlInjectionMessage;
using thesis::sqlInjectionReply;

#define SERVER_ADDRESS "0.0.0.0:50051"

class vulnerabilitiesServiceImplementation final : public vulnerabilitiesService::Service
{
public:
	Status unsecuredCredentials(ServerContext* context, const basicMessage* msg, basicMessage* rpl) override;
	Status protobufErrorDisclosure(ServerContext* context, const errDisclosureMessage* msg, errDisclosureReply* rpl) override;
	Status sqlInjection(ServerContext* context, const sqlInjectionMessage* msg, ServerWriter<sqlInjectionReply>* writer) override;
};

void test()
{
	try{
	std::cout << "connection test" << std::endl << std::endl;
	sql::Driver *driver;
	sql::Connection *conn;
	sql::Statement *stmt = nullptr;
	sql::PreparedStatement *prepStmt;
	sql::ResultSet *res;
	
	
	
	driver = get_driver_instance();
	conn = driver->connect("tcp://127.0.0.1", "admin", "passwd");
	conn->setSchema("users");
	
	prepStmt = conn->prepareStatement("SELECT * FROM _users WHERE id=?");
	prepStmt->setString(1, "1 OR 1=1");
	res = prepStmt->executeQuery();
	
	//stmt = conn->createStatement();
	//res = stmt->executeQuery("SELECT * FROM _users WHERE id = 1 OR 1=1");
	
	while(res->next()){
		std::cout << res->getString(2) << std::endl;	
	}

	delete res;
	delete stmt;
	delete prepStmt; 
	delete conn;
	} catch(sql::SQLException& e) {
		std::cout << "Something went wrong" << std::endl;
	}
}

std::string executeQuery(const char* query)
{
	std::array<char, 128> buffer;
	std::string result;
	std::unique_ptr<FILE, decltype(&pclose)> pipe(popen(query, "r"), pclose);
	if(!pipe)
		throw std::runtime_error("popen() failed!");
		
	while(nullptr != fgets(buffer.data(), buffer.size(), pipe.get()))
		result += buffer.data();
	return result;
}

Status vulnerabilitiesServiceImplementation::unsecuredCredentials(ServerContext* context, const basicMessage* msg, basicMessage* rpl)
{
	test();
	std::string text(msg->text());
	std::cout << "Recieved message: " << text << std::endl;
	
	rpl->set_text(text);
	
	return Status::OK;
}

#define DATA_NUMBER 3
Status vulnerabilitiesServiceImplementation::protobufErrorDisclosure(ServerContext* context, const errDisclosureMessage* msg, errDisclosureReply* rpl)
{
	int ID = msg->id();
	std::string query = "sudo echo \"SELECT * FROM _users WHERE id = ";
	query += std::to_string(ID);
	query += "\" | sudo mysql users";
	std::cout << "Querying database for: " << query.substr(10, 35) << std::endl << std::endl;
	std::stringstream result(executeQuery(query.c_str()));
	
	std::cout << "Fields given to us by database: ";
	
	std::string rubbishBin;
	result >> rubbishBin;
	std::cout << rubbishBin << ", ";
	result >> rubbishBin;
	std::cout << rubbishBin << ", ";
	result >> rubbishBin;
	std::cout << rubbishBin << "." << std::endl << std::endl;
	std::cout << "Including passwordhash (field \"password\", containing passwordhash)! That is something we do not want to be sending automatically" << std::endl << std::endl;

	const Descriptor* desc = rpl->GetDescriptor();
	const Reflection* refl = rpl->GetReflection();

	std::string sPlaceholder("");
	int iPlaceholder = -1;
	
	for(int i = 0; i < desc->field_count(); ++i){
		const FieldDescriptor* field = desc->field(i);
		
		if(!field->is_repeated()){
			result >> sPlaceholder;
			std::cout << "Automatically handling field: " << field->name() << std::endl;
			switch(field->type()){
			  case FieldDescriptor::TYPE_STRING: 
			  	refl->SetString(rpl, field, std::move(sPlaceholder));
			  	break;
			  case FieldDescriptor::TYPE_INT32:
			  	iPlaceholder = atoi(sPlaceholder.c_str());
			  	refl->SetInt32(rpl, field, std::move(iPlaceholder));
			  	break;  
			}
		}
	}

	return Status::OK;
}

Status vulnerabilitiesServiceImplementation::sqlInjection(ServerContext* context, const sqlInjectionMessage* msg, ServerWriter<sqlInjectionReply>* writer)
{
	std::string username = msg->username();
	
	std::string query = "sudo echo \"SELECT id, username FROM _users WHERE username = " + username + "\" | sudo mysql users";
	std::cout << "Asking query: " << std::endl << query.substr(10, 73) << std::endl;
	std::stringstream result(executeQuery(query.c_str()));

	std::string rubbishBin("");
	result >> rubbishBin;
	result >> rubbishBin;

	int id = -1;
	std::string user("");
	while(result >> id){
		result >> user;

		sqlInjectionReply rpl;
		rpl.set_id(id);
		rpl.set_username(user);
		writer->Write(rpl);
	}
	
	return Status::OK;
}

std::string readKeycert(const char* filename)
{
	std::ifstream file(filename);
	std::string ret((std::istreambuf_iterator<char>(file)),
			 std::istreambuf_iterator<char>());
	
//	std::getline(file, ret);
	file.close();
	
	std::cout << std::endl << ret << std::endl;
	
	return ret;
}

int main()
{
	vulnerabilitiesServiceImplementation serviceImplementation;
	ServerBuilder serverBuilder;

	std::string serverCert = readKeycert("server.crt");
	std::string serverKey = readKeycert("server.key");
	std::string caCert = readKeycert("ca.crt");
	
	if(serverCert.empty() || serverKey.empty() || caCert.empty())
		std::cout << "Houston" << std::endl;
		
	
	grpc::SslServerCredentialsOptions sslOptions;
	sslOptions.pem_key_cert_pairs.push_back({serverKey, serverCert});
	sslOptions.pem_root_certs = caCert;
	
	
	
	//grpc::SslServerCredentialsOptions::PemKeyCertPair pkcp;
	//pkcp.private_key = serverKey;
	//pkcp.cert_chain = serverCert;
	
	//std::cout << "I am here" << std::endl;
	
	
	//std::shared_ptr<grpc::ServerCredentials> creds = grpc::SslServerCredentials(sslOptions);
	
	//std::cout << "I am here" << std::endl;
	
	serverBuilder.AddListeningPort(SERVER_ADDRESS, grpc::SslServerCredentials(sslOptions));
	
	serverBuilder.RegisterService(&serviceImplementation);
	std::unique_ptr<Server> server(serverBuilder.BuildAndStart());
	std::cout << "Vulnerable server started" << std::endl;
	server->Wait();
}

/*
int main()
{
	vulnerabilitiesServiceImplementation serviceImplementation;
	ServerBuilder serverBuilder;
	
	
	
	constexpr char* kServerCertPath = "server.crt";
	constexpr char* kServerKeyPath = "server.key";
	//constexpr char* kCaCertPath = "ca.crt";
	grpc::SslCredentialsOptions sslOptions;
	sslOptions.pem_key_cert_pairs.push_back({ReadFile(kServerKeyPath), ReadFile(kServerCertPath)});
	sslOptions.pem_root_certs = ReadFile(kCaCertPath);
	
	//serverBuilder.AddListeningPort(SERVER_ADDRESS, grpc::InsecureServerCredentials());
	
	serverBuilder.AddListeningPort(SERVER_ADDRESS, grpc::SslServerCredentials(sslOptions));	
	serverBuilder.RegisterService(&serviceImplementation);
	std::unique_ptr<Server> server(serverBuilder.BuildAndStart());
	std::cout << "Vulnerable server started" << std::endl;
	server->Wait();
}*/

