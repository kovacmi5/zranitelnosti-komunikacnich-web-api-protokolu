#include <iostream>
#include <memory>
#include <string>
#include <initializer_list>

#include <mysql_driver.h>
#include <mysql_connection.h>
#include <cppconn/connection.h>
#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/prepared_statement.h>

#include "queryBuilder.h"

///Class responsible for holding database connection and server as an interface for queries
class CMySQLConnectionInterface
{
public:
	///Constructor of the class
	///Upon constructing class, connects to database by credentials
	///@Param IP: ip address of the database
	///@Param username: user credential -- username
	///@Param pswd: user credential -- password
	CMySQLConnectionInterface(const std::string& IP, const std::string& username, const std::string& pswd);

	~CMySQLConnectionInterface();

	///Function for setting the database schema to the one beeing worked upon
	///@param schema: name of the schema
	void setSchema(const std::string& schema);
	
	///Function serving as interface for SELECT
	///@param collums: name of the collums user wants selected
	///@param table: table name from which to SELECT. Accepts unions
	///@param constraints: specifies constraints for the WHERE clause
	///@param constraintValues: specifies constraint values
	///  for the WHERE clause. Must be in order of the previous parameter
	///  format: =1... "Contains" and such not implemented due to time reasons
	///@param order: ORDER BY order
	///@param limit: LIMIT = limit
	std::string Select(const std::initializer_list<std::string>& collums, 
			   const std::string& table, 
			   const std::initializer_list<std::string>& constraints = std::initializer_list<std::string>(), 
			   const std::initializer_list<std::string>& constraintValues = std::initializer_list<std::string>(), 
			   const std::string& order = "", 
			   int limit = 0);

	///Function serving as interface for INSERT
	///@param table: table name to which INSERT is happening. Accepts unions
	///@param collums: name of the collums user wants selected
	///@param values: specifies constraint values
	std::string Insert(const std::string& table,
			   const std::initializer_list<std::string>& collums,
			   const std::initializer_list<std::string>& values);

	///Function serving as interface for UPDATE
	///@param table: table name from which to UPDATE. Accepts unions
	///@param collums: name of the collums user wants update
	///@param values: specifies collum values
	///  format: =1...
	///@param constraints: specifies constraints for the WHERE clause
	///@param constraintValues: specifies constraint values
	///  format: =1...
	std::string Update(const std::string& table,
			   const std::initializer_list<std::string>& collums,
			   const std::initializer_list<std::string>& values,
			   const std::initializer_list<std::string>& constraints = std::initializer_list<std::string>(),
   			   const std::initializer_list<std::string>& constraintValues = std::initializer_list<std::string>());

	///Function serving as interface for DELETE
	///@param table: table name from which to delete from. Accepts unions
	///@param collums: name of the collums user wants delete
	///@param values: specifies collum values
	std::string Delete(const std::string& table,
			   const std::initializer_list<std::string>& collums,
			   const std::initializer_list<std::string>& values);

private:
	///Variables defined in MySQL documentation: https://dev.mysql.com/doc/
	///Hold the driver and connection pointer needed for succesfull connection
	sql::Driver* m_Driver;
	std::unique_ptr<sql::Connection> m_Connection;

	///Name of the current working schema. Changed by function SetSchema
	std::string m_Schema;

	///Class delegated for building queries outside of the database, contained in queryBuilder.h
	CQueryBuilder m_QueryBuilder;
};
