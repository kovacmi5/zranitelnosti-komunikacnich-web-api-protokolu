#include <iostream>
#include <memory>
#include <string>
#include <initializer_list>

class CQueryBuilder
{
public:
	CQueryBuilder();
	CQueryBuilder(const char* base);
	CQueryBuilder(const std::string& base);

	///Function serving for building SELECT
	///@param collums: name of the collums user wants selected
	///@param table: table name from which to SELECT. Accepts unions
	///@param constraints: specifies constraints for the WHERE clause
	///@param constraintValues: specifies constraint values
	///  for the WHERE clause. Must be in order of the previous parameter
	///  format: =1... "Contains" and such not implemented due to time reasons
	///@param order: ORDER BY order
	///@param limit: LIMIT = limit
	std::string Select(const std::initializer_list<std::string>& collums, 
			   const std::string& table, 
			   const std::initializer_list<std::string>& constraints = std::initializer_list<std::string>(), 
			   const std::initializer_list<std::string>& constraintValues = std::initializer_list<std::string>(), 
			   const std::string& order = "", 
			   int limit = 0);

	///Function serving as interface for building INSERT
	///@param table: table name to which INSERT is happening. Accepts unions
	///@param collums: name of the collums user wants selected
	///@param values: specifies constraint values
	std::string Insert(const std::string& table,
			   const std::initializer_list<std::string>& collums,
			   const std::initializer_list<std::string>& values);

	///Function serving as interface for building UPDATE
	///@param table: table name from which to UPDATE. Accepts unions
	///@param collums: name of the collums user wants update
	///@param values: specifies collum values
	///  format: =1...
	///@param constraints: specifies constraints for the WHERE clause
	///@param constraintValues: specifies constraint values
	///  format: =1...
	std::string Update(const std::string& table,
			   const std::initializer_list<std::string>& collums,
			   const std::initializer_list<std::string>& values,
			   const std::initializer_list<std::string>& constraints = std::initializer_list<std::string>(),
   			   const std::initializer_list<std::string>& constraintValues = std::initializer_list<std::string>());

	///Function serving as interface for building DELETE
	///@param table: table name from which to delete from. Accepts unions
	///@param collums: name of the collums user wants delete
	///@param values: specifies collum values
	std::string Delete(const std::string& table,
			   const std::initializer_list<std::string>& collums,
			   const std::initializer_list<std::string>& values);

	///Function that returns last query asked
	std::string getQuery();	

	///Function for restarting the query building procces
	void clear();
private:
	///Function for appending text to query. Basically += with extra whitespace formatting 
	///@param toAppend: text to be appended
	void append(char* toAppend);

	///Function for appending text to query. Basically += with extra whitespace formatting 
	///@param toAppend: text to be appended
	void append(std::string toAppend);

	///Function for appending everything else but text to query
	///@param toAppend: variable to be appended
	template<typename T> void append(const std::initializer_list<T>& toAppend);

	///Function for appending parameters to clauses such as in INSERT
	///@param parameters: name of the parameters to be appended
	///@param parameteValues: values of the parameters to be appended
	///  format: =1...
	template<typename T> void appendParameters(const std::initializer_list<T>& parameters, const std::initializer_list<T>& parameterValues); 

	///Function for appending parameters to WHERE clause in its correct format
	///@param parameters: name of the parameters to be appended
	///@param parameteValues: values of the parameters to be appended
	///  format: =1...
	template<typename T> void appendConstraints(const std::initializer_list<T>& constraints, const std::initializer_list<T>& constraintValues); 
	
	///Holds the query in std::string
	std::string m_Query;
};
