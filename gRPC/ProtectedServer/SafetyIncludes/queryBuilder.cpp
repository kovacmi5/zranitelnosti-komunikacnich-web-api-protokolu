#include "queryBuilder.h"

CQueryBuilder::CQueryBuilder(): m_Query(std::string("")){}
CQueryBuilder::CQueryBuilder(const char* base): m_Query(base){}
CQueryBuilder::CQueryBuilder(const std::string& base): m_Query(base){}

void CQueryBuilder::append(char* toAppend) {m_Query += toAppend; m_Query += ' ';}
void CQueryBuilder::append(std::string toAppend) {m_Query += toAppend; m_Query += ' ';}
template<typename T> void CQueryBuilder::append(const std::initializer_list<T>& toAppend) 
{
	bool writeDash = false;
	for(auto& i: toAppend){
		if(writeDash) 
			m_Query += ", ";
		m_Query += std::string(i);
	
		writeDash = true;
	}
	m_Query += ' ';
}

template<typename T> void CQueryBuilder::appendParameters(const std::initializer_list<T>& parameters, const std::initializer_list<T>& parameterValues)
{
	if(parameters.size() != parameterValues.size())
		return;
	
	auto c = parameters.begin();
	auto v = parameterValues.begin();
	
	bool writeDash = false;
	
	for(int i = 0; i < parameters.size(); ++i){
		if(writeDash)
			m_Query += " , ";
		
		m_Query += *(c + i);
		m_Query += " = ";
		m_Query += *(v + i);
		writeDash = true;	
	}
	m_Query += " ";

} 

template<typename T> void CQueryBuilder::appendConstraints(const std::initializer_list<T>& constraints, const std::initializer_list<T>& constraintValues) 
{
	if(constraints.size() != constraintValues.size())
		return;
	
	auto c = constraints.begin();
	auto v = constraintValues.begin();
	bool writeAND = false;
	
	for(int i = 0; i < constraints.size(); ++i){
		if(writeAND)
			m_Query += " AND ";
		
		m_Query += *(c + i);
		m_Query += " ";
		m_Query += (*(v + i))[0];
		m_Query += " ?";
		writeAND = true;	
	}
	m_Query += " ";
}

std::string CQueryBuilder::getQuery() {return m_Query;}

void CQueryBuilder::clear() {m_Query.clear();}

std::string CQueryBuilder::Select(const std::initializer_list<std::string>& collums, 
			    	  const std::string& table, 
			    	  const std::initializer_list<std::string>& constraints, 
			    	  const std::initializer_list<std::string>& constraintValues, 
			    	  const std::string& order, 
			    	  int limit)
{
	clear();
	append(std::string("SELECT"));
	append(collums);
	append(std::string("FROM"));
	append(table);
	
	if(0 != constraints.size()){
		append(std::string("WHERE"));
		appendConstraints(constraints, constraintValues);
	}
	
	if("" != order){
		append(std::string("ORDER BY ") + order);
	}
	
	if(0 != limit){
		append(std::string("LIMIT ") + std::to_string(limit));
	}
	
	return m_Query;
}

std::string CQueryBuilder::Insert(const std::string& table,
				  const std::initializer_list<std::string>& collums,
				  const std::initializer_list<std::string>& values)
{
	if(collums.size() != values.size()) return "";	//throw

	clear();
	append(std::string("INSERT INTO"));
	append(table);
	append(std::string("("));
	append(collums);
	append(std::string(")"));
	append(std::string("VALUES"));
	append(std::string("("));
	bool writeComma = false;
	for(auto& i: values) {
		if(writeComma)
			append(std::string(","));
		append(std::string("?"));
		writeComma = true;
	}
	append(std::string(")"));
	
	return m_Query;
}

std::string CQueryBuilder::Update(const std::string& table,
		   		  const std::initializer_list<std::string>& collums,
			          const std::initializer_list<std::string>& values,
				  const std::initializer_list<std::string>& constraints,
				  const std::initializer_list<std::string>& constraintValues)
{
	if(collums.size() != values.size()) return "";
	if(constraints.size() != constraintValues.size()) return "";
	
	clear();
	append(std::string("UPDATE"));
	append(table);
	append(std::string("SET"));
	bool writeComma = false;
	for(auto& i: collums) {
		if(writeComma)
			append(std::string(","));
		append(i);
		append(std::string("= ?"));
		writeComma = true;
	}

	
	if(0 != constraints.size()){
		append(std::string("WHERE"));
		appendConstraints(constraints, constraintValues);
	}
		
	return m_Query;
}	 

std::string CQueryBuilder::Delete(const std::string& table,
				  const std::initializer_list<std::string>& collums,
				  const std::initializer_list<std::string>& values)
{
	clear();
	append(std::string("DELETE FROM"));
	append(table);
	if(0 != values.size()){	
		append(std::string("WHERE"));
		appendConstraints(collums, values);
	}
	
	return m_Query;
}

