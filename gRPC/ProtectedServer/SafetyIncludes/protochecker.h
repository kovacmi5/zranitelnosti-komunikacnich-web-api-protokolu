#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <set>
#include <array>
#include <algorithm>
#include <sstream>
#include <cstring>

///Class made to cover all the methods for successfull proto file check 
///The single public method is the only one used and takes care of the whole procedure		
class CProtoChecker
{
public:
	CProtoChecker();
	~CProtoChecker();
		
	///Function for automation of checking proto file for potentially dangerous field
	///Takes care of administration - loading the configuration file, opening logging file, 
	///searching for proto files...
	///@param confFile: name of the configuration file .config, specified in thesis pdf
	///@param deny: number of dangerous fields found, return value
	///@param warn: number of potentially dangerous fields found, return value
	///@return: error code. 0 upon success
	int check(const char* confFile, int& deny, int& warn);
private:

	///Function delegated for reading a single proto file and compare it with the word list
	///@param fileName: name of the proto file beeing read
	///@param denySet: list of dangerous fields given by config file
	///@param warnSet: list of potentially dangerous fields given by config file
	///@param deny: number of dangerous fields found, return value
	///@param warn: number of potentially dangerous fields found, return value
	///@return: true upon success, false upon failure
	bool checkFile(const char* fileName, const std::set<std::string>& denySet, const std::set<std::string>& warnSet, int& deny, int& warn);

	///Function for parsing proto file into sets
	///@param fileName: name of the proto file beeing read
	///@param denySet: list of dangerous fields, return value
	///@param warnSet: list of potentially dangerous fields, return value
	///@return: true upon success, false upon failure
	bool createSets(std::set<std::string>& denySet, std::set<std::string>& warnSet);

	///Function for reading a line from config file
	///ReadFile with addedd functionality for comments
	std::string readLine(std::istream& stream);

	///Functions used for string trimming, getting rid of whitespaces
	void trim(std::string& toBeTrimmed);
	void lTrim(std::string& toBeTrimmed);
	void rTrim(std::string& toBeTrimmed);

	///Variables for configuration file parsing, proto file parsing and logging, in this order
	std::ifstream m_ConfStream;
	std::string m_ConfString;

	std::ifstream m_ProtoStream;
	std::string m_ProtoString;
	
	std::ofstream m_LogStream;
	
	///true if logging option is set
	bool m_Logging = false;
};
