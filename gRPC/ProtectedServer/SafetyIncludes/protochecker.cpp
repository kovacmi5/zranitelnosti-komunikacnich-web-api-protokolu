#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <set>
#include <array>
#include <algorithm>
#include <sstream>
#include <cstring>

#define COMMENT_SIGN '#'
#define NUMBER_OF_CONFIG_PARTS 4

///Parts of the configuration file in its order
constexpr const char* configParts[NUMBER_OF_CONFIG_PARTS] = {
"proto:", "log:", "deny:", "warn:" 
};

///Function to transform std::string into an integer for switch purposes
constexpr unsigned int hash(const char* data, int size) noexcept
{
	unsigned int constant = 5381;
	
	for(const char* c = data; c < data + size; ++c){
		constant = ((constant << 5) + constant) + (unsigned char)*c;
	}
	
	return constant;
}

CProtoChecker::CProtoChecker() = default;
CProtoChecker::~CProtoChecker()
{
	m_ConfStream.close();
	m_ProtoStream.close();
	m_LogStream.close();
}

#define CONTAINS "contains:"
#define MATCH "match:"

bool CProtoChecker::checkFile(const char* fileName, const std::set<std::string>& denySet, const std::set<std::string>& warnSet, int& deny, int& warn)
{
	std::ifstream protoStream(fileName);
	if(! protoStream.is_open()) {
		std::cout << "Error opening proto file" << std::endl;
		return false;
	}
	std::string head;
	while(protoStream >> head){

		//ignoring everything until a message declaration is found
		if("message" != head) continue;
		
		//read message name and ensure the "{" sign has been read	
		protoStream >> head;		
		if(-1 == head.find("{")) {protoStream >> head;}	
		
		//iterate through the whole message
		while(true){
		
			//the breaking point. If break does not happen, head contains the message type now
			protoStream >> head;
			if(-1 != head.find("}")) break;		
			
			//read message field name
			protoStream >> head;
			
			//iterate through the whole deny set in order to find word matching message field name
			//parsing the contains/match first, then comparing the rest of the string to word i am trying to find
			for(auto& i: denySet){		
				if(-1 != i.find(CONTAINS)){
					if(head == i.substr(strlen(CONTAINS) + 1)){
						deny++; std::cout << i.substr(strlen(CONTAINS) + 1) << " == " << head << std::endl;
					}
				}
				else if(-1 != i.find(MATCH)){
					if(-1 != head.find(i.substr(strlen(MATCH) + 1))){
						deny++; std::cout << i.substr(strlen(MATCH) + 1)  << " matches " << head << std::endl;
					}
				}
				else {std::cout << "WRONG FORMATTING ON LINE" << i << std::endl; if(m_Logging) m_LogStream << "WRONG FORMATTING ON LINE" << i << '\n';}
			}
			
			//iterate through the whole warn set in order to find word matching message field name
			//parsing the contains/match first, then comparing the rest of the string to word i am trying to find
			for(auto& i: warnSet){
				if(-1 != i.find(CONTAINS)){
					if(head == i.substr(strlen(CONTAINS) + 1)){
						warn++; std::cout << i.substr(strlen(CONTAINS) + 1) << " == " << head << std::endl;
					}
				}
				else if(-1 != i.find(MATCH)){
					if(-1 != head.find(i.substr(strlen(MATCH) + 1))){
						warn++; std::cout << i.substr(strlen(MATCH) + 1) << " matches " << head << std::endl;
					}
				}
				else {std::cout << "WRONG FORMATTING ON LINE" << i << std::endl; if(m_Logging) m_LogStream << "WRONG FORMATTING ON LINE" << i << '\n';}
			}
			//looking for delimeter - parsing the = number 
			while(protoStream >> head){		
				if(-1 != head.find(";")) break;
			}
		}
	}
	protoStream.close();
	return 0;	
}

#define FINE 0
#define CONF_NOT_FOUND 1
#define EXPECTED_PROTO 2
#define WRONG_FORMATTING_PROTOS 3
#define WRONG_FORMATTING_SETS 4

int CProtoChecker::check(const char* confFile, int& deny, int& warn)
{
	std::cout << "Proto checker v 1.0 end of sleep phase: " << std::endl; 
	m_ConfStream.open(confFile);
	if(! m_ConfStream.is_open()){
		std::cout << "Configuration file not found" << std::endl;
		return CONF_NOT_FOUND;
	}
	
	std::string head = readLine(m_ConfStream);
	if(0 != head.compare(configParts[0])){
		std::cout << "Expected proto files at the beggining (proto:)" << std::endl;
		return EXPECTED_PROTO;
	}

	std::vector<std::string> protoFiles;
	bool toBreak = false;
	while(true){
		head = readLine(m_ConfStream);
		for(int i = 1; i < NUMBER_OF_CONFIG_PARTS; ++i){
			if(configParts[i] == head) {toBreak = true; break;}
			if("" == head){
				std::cout << "WRONG FILE FORMATTING WHILE READING PROTOS" << std::endl;
				return WRONG_FORMATTING_PROTOS;
			}
		}
		if(toBreak) break;	
		protoFiles.emplace_back(head);
	}
	if(configParts[1] == head){
		head = readLine(m_ConfStream);
		m_LogStream.open(head.c_str(), std::ios::app );
		if(m_LogStream.is_open())
			m_Logging = true; 
	}

	std::set<std::string> denySet;
	std::set<std::string> warnSet;
	if(! createSets(denySet, warnSet)){
		std::cout << "WRONG FORMATING ON PROTO FILE" << std::endl;
		return WRONG_FORMATTING_SETS;
	}
	
	for(auto& i: protoFiles){
		bool success = checkFile(i.c_str(), denySet, warnSet, deny, warn);
		if(! success){
			if(m_Logging)
				m_LogStream << "Checking file " << i << " showed error " << "\n";
			std::cout << "Checking file " << i << " showed error" << std::endl;
		}
	}
	
	return FINE;
}

bool CProtoChecker::createSets(std::set<std::string>& denySet, std::set<std::string>& warnSet)
{
	std::string head = readLine(m_ConfStream);
	bool toBreak = false;
	int i = 0;
	while(true)
	{
		switch(hash(head.c_str(), head.size())){
			case hash(configParts[2], strlen(configParts[2])):

				while(true){
					head = readLine(m_ConfStream);
					i++;
					if(configParts[3] != head && "" != head){
						if(-1 == head.find("match:") && -1 == head.find("contains:")){
							std::cout << "WRONG FORMATTING IN PROTO FILE, LINE: " << head << std::endl;
							if(m_Logging)
								m_LogStream << "WRONG FORMATTING IN PROTO FILE, LINE: " << head << "\n";
						}
						else
							denySet.insert(head);
					}
					else break;
				}
			case hash(configParts[3], strlen(configParts[3])):
				while(true){
					head = readLine(m_ConfStream);
					i++;
					if(configParts[2] != head && "" != head){
						if(-1 == head.find("match:") && -1 == head.find("contains:")){
							std::cout << "WRONG FORMATTING 2 IN PROTO FILE, LINE: " << head << std::endl;
							if(m_Logging)
								m_LogStream << "WRONG FORMATTING IN PROTO FILE, LINE: " << head << "\n";
						}
						else
							warnSet.insert(head);
						
					}
					else break;
				}
			default: toBreak = true; std::cout << "PARSED " << i << " LINES OF PROTO FILE" << std::endl; break;
		}
		if(toBreak)
			break;
	}
	return true;
}

std::string CProtoChecker::readLine(std::istream& stream) 
{
	std::string ret;
	char buffer[256] = { 0 };
	while(stream.getline(buffer, 256)){
		ret = buffer;
		trim(ret);
		if("" == ret)
			continue;
		if(COMMENT_SIGN != ret.at(0)){
			break;
		}
	}
	
	return ret;
}

void CProtoChecker::trim(std::string& toBeTrimmed)
{
	lTrim(toBeTrimmed);
	rTrim(toBeTrimmed);
}

void CProtoChecker::lTrim(std::string& toBeTrimmed)
{
	auto it = std::find_if(toBeTrimmed.begin(), toBeTrimmed.end(), 
				[](char ch){return !std::isspace<char>(ch, std::locale::classic());});
	
	toBeTrimmed.erase(toBeTrimmed.begin(), it);
}

void CProtoChecker::rTrim(std::string& toBeTrimmed)
{
	auto it = std::find_if(toBeTrimmed.rbegin(), toBeTrimmed.rend(), 
				[](char ch){return !std::isspace<char>(ch, std::locale::classic());});
	
	toBeTrimmed.erase(it.base(), toBeTrimmed.end());
}
