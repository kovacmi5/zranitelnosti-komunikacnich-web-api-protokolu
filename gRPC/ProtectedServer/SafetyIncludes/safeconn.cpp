std::string readKeycert(const char* filename)
{
	std::ifstream file(filename);
	std::string ret((std::istreambuf_iterator<char>(file)),
			 std::istreambuf_iterator<char>());
	file.close();
	
	return ret;
}

bool safeConn(ServerBuilder& serverBuilder, const char* serverAddress ,const char* serverCert, const char* serverKey, const char* caCert)
{
	std::string lserverCert = readKeycert(serverCert);
	std::string lserverKey = readKeycert(serverKey);
	std::string lcaCert = readKeycert(caCert);
	
	if(lserverCert.empty() || lserverKey.empty() || lcaCert.empty())
		return false;
		
	grpc::SslServerCredentialsOptions sslOptions;
	sslOptions.pem_key_cert_pairs.push_back({lserverKey, lserverCert});
	sslOptions.pem_root_certs = lcaCert;
	
	serverBuilder.AddListeningPort(serverAddress, grpc::SslServerCredentials(sslOptions));

    return true;
}
