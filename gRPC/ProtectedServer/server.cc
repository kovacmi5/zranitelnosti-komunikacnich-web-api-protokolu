#include <grpc/grpc.h>
#include <grpcpp/server.h>
#include <grpcpp/server_builder.h>
#include <grpcpp/server_context.h>
#include <grpcpp/security/server_credentials.h>

#include "thesis.grpc.pb.h"

#include <mysql_driver.h>
#include <mysql_connection.h>
#include <cppconn/connection.h>
#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/prepared_statement.h>
#include <string>
#include <stdexcept>
#include <memory>

#include "SafetyIncludes/mySQLConnectionInterface.h"
#include "SafetyIncludes/protochecker.h"
#include "safeconn.cpp"

using grpc::Server;
using grpc::ServerBuilder;
using grpc::ServerContext;
using grpc::ServerReader;
using grpc::ServerWriter;
using grpc::ServerReaderWriter;
using grpc::Status;
using google::protobuf::Descriptor;
using google::protobuf::FieldDescriptor;
using google::protobuf::Reflection;

using thesis::vulnerabilitiesService;
using thesis::authMessage;
using thesis::authReply;
using thesis::basicMessage;
using thesis::errDisclosureMessage;
using thesis::errDisclosureReply;
using thesis::sqlInjectionMessage;
using thesis::sqlInjectionReply;

#define SERVER_ADDRESS "0.0.0.0:50051"

///class representing servers public methods. 
///Public methods implement services described in thesis.proto
///Private methods are helper methods
class vulnerabilitiesServiceImplementation final : public vulnerabilitiesService::Service
{
public:
	Status unsecuredCredentials(ServerContext* context, const basicMessage* message, basicMessage* reply) override;
	Status protobufErrorDisclosure(ServerContext* context, const errDisclosureMessage* message, errDisclosureReply* reply) override;
	Status sqlInjection(ServerContext* context, const sqlInjectionMessage* message, ServerWriter<sqlInjectionReply>* writer) override;
	Status authenticate(ServerContext* context, const authMessage* message, authReply* reply) override;
private:
	std::set<std::string> m_AuthenticatedUsers;
};

///Naive Function used to demonstrate protobuf error disclosure
///Function sends user id to database
///and then adds everything it can and something that cannot to reply 
///@param context: grpc::Context. Never used.
///@param username: thesis::basicMessage, contains test string
///@param reply: thesis::basicMessage, sends test string back via this message
Status vulnerabilitiesServiceImplementation::unsecuredCredentials(ServerContext* context, const basicMessage* message, basicMessage* reply)
{	
	if(m_AuthenticatedUsers.end() == m_AuthenticatedUsers.find(message->username())){
		std::cout << "Authentication error" << std::endl;
		return Status::OK;
	}
	
	std::string text(message->text());
	std::cout << "Recieved message: " << text << std::endl;
	std::cout << "Sending it back!" << std::endl;
	reply->set_text(text);
	return Status::OK;
}

#define NUMBER_OF_COLLUMS 3

///Naive Function used to demonstrate protobuf error disclosure
///Function sends user id to database
///and then adds everything it can and something that cannot to reply 
///@param context: grpc::Context. Never used.
///@param username: thesis::errorDisclosureMessage, contains id for query
///@param reply: thesis::errorDisclosureReply, server sends back info via this reply
Status vulnerabilitiesServiceImplementation::protobufErrorDisclosure(ServerContext* context, const errDisclosureMessage* message, errDisclosureReply* reply)
{
	if(m_AuthenticatedUsers.end() == m_AuthenticatedUsers.find(message->username())){
		std::cout << "Authentication error" << std::endl;
		return Status::OK;
	}
	
	int ret1 = 0, ret2 = 0; 
	CProtoChecker checker;
	checker.check("protochecker.config", ret1, ret2);
	if(ret1 > 0 || ret2 > 0) {std::cout << "not safe!" << std::endl; return Status::OK;}

	std::vector<std::string> databaseOutputs;
	try{
		sql::Driver *driver = get_driver_instance();
		std::unique_ptr<sql::Connection> connection(driver->connect("tcp://127.0.0.1", "admin", "passwd"));
		connection->setSchema("users");
		std::unique_ptr<sql::Statement> statement(connection->createStatement());
		std::cout << message->id() << std::endl;
		std::cout << std::string("SELECT * FROM _users WHERE id = '" + std::to_string(message->id()) + "'").c_str() << std::endl;
		
		std::unique_ptr<sql::ResultSet> result(statement->executeQuery(
			std::string("SELECT * FROM _users WHERE id = '" + std::to_string(message->id()) + "'").c_str()));
		while(result->next()){
			for(int i = 1; i <= NUMBER_OF_COLLUMS; ++i){
				databaseOutputs.emplace_back(result->getString(i));
			}
		}
			
	} catch(sql::SQLException& e) {
		std::cout << "Something went wrong during protobuf err disclosure: " << e.what() << std::endl;
	}

	const Descriptor* desc = reply->GetDescriptor();
	const Reflection* refl = reply->GetReflection();

	std::string sPlaceholder("");
	int iPlaceholder = -1;
	
	for(int i = 0; i < desc->field_count(); ++i){
		const FieldDescriptor* field = desc->field(i);
		
		if(!field->is_repeated()){
			sPlaceholder = databaseOutputs[i];
			std::cout << "Automatically handling field: " << field->name() << std::endl;
			switch(field->type()){
			  case FieldDescriptor::TYPE_STRING: 
			  	refl->SetString(reply, field, std::move(sPlaceholder));
			  	break;
			  case FieldDescriptor::TYPE_INT32:
			  	iPlaceholder = atoi(sPlaceholder.c_str());
			  	refl->SetInt32(reply, field, std::move(iPlaceholder));
			  	break;  
			}
		}
	}

	return Status::OK;
}

///Naive Function used to demonstrate SQL injection
///Function sends user inputed username to database
///without prior user input validation. 
///Sends back the recieved content, again without validation.
///@param context: thesis::Context. Never used.
///@param username: thesis::sqlInjection message, contains username for query
///@param writer: grpc::messageStream, vector of thesis::sqlInjectionReplies
Status vulnerabilitiesServiceImplementation::sqlInjection(ServerContext* context, const sqlInjectionMessage* message, ServerWriter<sqlInjectionReply>* writer)
{
	if(m_AuthenticatedUsers.end() == m_AuthenticatedUsers.find(message->username())){
		std::cout << "Authentication error" << std::endl;
		return Status::OK;
	}

	std::string res;
	try{
		CMySQLConnectionInterface connectionInterface("tcp://127.0.0.1", "admin", "passwd");
		res = connectionInterface.select({"username"}, "_users", {"id"}, {"=" + message->id()});
	} catch(sql::SQLException& e) {
		std::cout << "Something went wrong during protobuf err disclosure: " << e.what() << std::endl;
	}

	sqlInjectionReply reply;
	reply.set_id(rand() % 19);		///string parsing not implemented due to time limit 	
	reply.set_username(user);
	writer->Write(reply);
	return Status::OK;
}

///Function for the simplest authentication
///Function pulls out a record from database according to parameter
///then compares to to pwd in parameter. WHERE username = '" + message->n() + "'
///@param a: grpc message containing username and pwd
Status vulnerabilitiesServiceImplementation::authenticate(ServerContext* context, const authMessage* message, authReply* reply)
{	
	try{
		sql::Driver *driver = get_driver_instance();
		std::unique_ptr<sql::Connection> connection(driver->connect("tcp://127.0.0.1", "admin", "passwd"));
		connection->setSchema("users");
		std::unique_ptr<sql::Statement> statement(connection->createStatement());
		//std::cout << std::string("SELECT * FROM _users").c_str() << std::endl;
		std::unique_ptr<sql::ResultSet> result( 
			statement->executeQuery(std::string("SELECT * FROM _users WHERE username = '" + message->n() + "'").c_str()));
			std::string pwd;// = result->getString("password");
		while(result->next()){
			pwd = result->getString("password");
		}
		if(pwd == message->p())
			m_AuthenticatedUsers.insert(message->n());
			
	} catch(sql::SQLException& e) {
		std::cout << "Something went wrong during authentication: " << e.what() << std::endl;
	}
	
	return Status::OK;
}

///Main function, used to simply start the server via gRPC API
int main()
{
	ServerBuilder serverBuilder;
	vulnerabilitiesServiceImplementation serviceImplementation;
	serverBuilder.RegisterService(&serviceImplementation);
	if(!safeConn(serverBuilder, SERVER_ADDRESS, "server.crt", "server.key", "ca.crt"))
		return 1;

	std::unique_ptr<Server> server(serverBuilder.BuildAndStart());
	std::cout << "Safe server started" << std::endl;
	server->Wait();
}
