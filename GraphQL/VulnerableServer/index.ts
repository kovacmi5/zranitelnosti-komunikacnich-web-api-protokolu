import { ApolloServer } from '@apollo/server';
import { startStandaloneServer } from '@apollo/server/standalone';
//import { exec } from 'child_process';
import { execSync } from 'child_process';

const typeDefs = `#graphql

  type Book {
    id: Int
    Title: String
    Author: [Author]
  },

  type Author {
    id: Int
    Name: String
    BookTitles: [String]
    Books: [Book]
  },

  type Query {
    books: [Book]
  },

  type Query {
    book(id: Int!): Book
  },

  type Query {
    authors: [Author]
  },
  
  type Query {
    author(id: Int!): Author
  },
  
  type Query {
    getBookFromDatabase(id: String!) : String
  }
`;

const books = [

  {
    id: 1,
    Title: 'The Awakening',
    Author: 'Kate Chopin',

  },
  {
    id: 2,
    Title: 'City of Glass',
    Author: 'Paul Auster',
  },
];

const authors = [

   {
     id: 1,
     Name: 'Kate Chopin',
     BookTitles: ['The Awakening', 'The Going back to sleep']
   },
   {
     id: 2,
     Name: 'Christopher Robins',
     BookTitles: ['The Christmas miracle', 'The easter catastrophe']
   },
];

async function executeQuery(stmt: String) {
    let query: string = 'echo "' + stmt + '" | sudo mysql books';
    var res = execSync(query);
    return res.toString();
}

const resolvers = {

  Query: {
    books: () => books,
    book(parent, args, contextValue, info){
    	return books.find((book) => book.id === args.id)
    },
    
    authors: () => authors,
    author(parent, args, contextValue, info){
    	return authors.find((author) => author.id === args.id)
    },
    
    async getBookFromDatabase(parent, args, contextValue, info){
    	const stmt = 'SELECT * FROM _books WHERE id = ' + args.id;
    	const data = await executeQuery(stmt);
    	return data;
    },
  },
  
  Author: {
    Books: (Author) => {
      return books.filter((book) => book.Author === Author.Name)
    }
  },
  Book: {
    Author: (book) => {
      return authors.filter((author) => author.BookTitles.indexOf(book.Title) > - 1)
    }
  }
};

const server = new ApolloServer({
  typeDefs,
  resolvers,
});

const { url } = await startStandaloneServer(server, {  listen: { port: 4000 },});

console.log(`Server ready at: ${url}`);
