#include <grpc/grpc.h>
#include <grpcpp/server.h>
#include <grpcpp/server_builder.h>
#include <grpcpp/server_context.h>
#include <grpcpp/security/server_credentials.h>

#include "thesis.grpc.pb.h"

#include <iostream>
#include <string>
#include <array>
#include <stdexcept>
#include <memory>
#include <sstream>

using grpc::Server;
using grpc::ServerBuilder;
using grpc::ServerContext;
using grpc::ServerReader;
using grpc::ServerWriter;
using grpc::ServerReaderWriter;
using grpc::Status;
using google::protobuf::Descriptor;
using google::protobuf::FieldDescriptor;
using google::protobuf::Reflection;


using thesis::vulnerabilitiesService;
using thesis::basicMessage;
using thesis::errDisclosureMessage;
using thesis::errDisclosureReply;
using thesis::sqlInjectionMessage;
using thesis::sqlInjectionReply;

#define SERVER_ADDRESS "0.0.0.0:50051"

class vulnerabilitiesServiceImplementation final : public vulnerabilitiesService::Service
{
public:
	Status unsecuredCredentials(ServerContext* context, const basicMessage* msg, basicMessage* rpl) override;
	Status protobufErrorDisclosure(ServerContext* context, const errDisclosureMessage* msg, errDisclosureReply* rpl) override;
	Status sqlInjection(ServerContext* context, const sqlInjectionMessage* msg, ServerWriter<sqlInjectionReply>* writer) override;
};

std::string executeQuery(const char* query)
{
	std::array<char, 128> buffer;
	std::string result;
	std::unique_ptr<FILE, decltype(&pclose)> pipe(popen(query, "r"), pclose);
	if(!pipe)
		throw std::runtime_error("popen() failed!");
		
	while(nullptr != fgets(buffer.data(), buffer.size(), pipe.get()))
		result += buffer.data();
	std::cout << result << std::endl;
	return result;
}

Status vulnerabilitiesServiceImplementation::unsecuredCredentials(ServerContext* context, const basicMessage* msg, basicMessage* rpl)
{
	std::string text(msg->text());
	std::cout << "Recieved message: " << text << std::endl;
	
	rpl->set_text(text);
	
	return Status::OK;
}

#define DATA_NUMBER 3
Status vulnerabilitiesServiceImplementation::protobufErrorDisclosure(ServerContext* context, const errDisclosureMessage* msg, errDisclosureReply* rpl)
{
	int ID = msg->id();
	std::string query = "sudo echo \"SELECT * FROM _users WHERE id = ";
	query += std::to_string(ID);
	query += "\" | sudo mysql users";
	
	std::stringstream result(executeQuery(query.c_str()));
	
	std::string rubbishBin;
	result >> rubbishBin;
	result >> rubbishBin;
	result >> rubbishBin;

	const Descriptor* desc = rpl->GetDescriptor();
	const Reflection* refl = rpl->GetReflection();

	std::string sPlaceholder("");
	int iPlaceholder = -1;
	
	for(int i = 0; i < desc->field_count(); ++i){
		const FieldDescriptor* field = desc->field(i);
		
		if(!field->is_repeated()){
			result >> sPlaceholder;
			switch(field->type()){
			  case FieldDescriptor::TYPE_STRING: 
			  	refl->SetString(rpl, field, std::move(sPlaceholder));
			  	break;
			  case FieldDescriptor::TYPE_INT32:
			  	iPlaceholder = atoi(sPlaceholder.c_str());
			  	refl->SetInt32(rpl, field, std::move(iPlaceholder));
			  	break;  
			}
		}
	}

	return Status::OK;
}

Status vulnerabilitiesServiceImplementation::sqlInjection(ServerContext* context, const sqlInjectionMessage* msg, ServerWriter<sqlInjectionReply>* writer)
{
	std::string username = msg->username();
	
	std::string query = "sudo echo \"SELECT id, username FROM _users WHERE username = " + username + "\" | sudo mysql users";
	std::cout << query << std::endl;
	std::stringstream result(executeQuery(query.c_str()));

	std::string rubbishBin("");
	result >> rubbishBin;
	result >> rubbishBin;

	int id = -1;
	std::string user("");
	while(result >> id){
		result >> user;

		sqlInjectionReply rpl;
		rpl.set_id(id);
		rpl.set_username(user);
		writer->Write(rpl);
	}
	
	return Status::OK;
}

int main()
{
	vulnerabilitiesServiceImplementation serviceImplementation;
	ServerBuilder serverBuilder;
	serverBuilder.AddListeningPort(SERVER_ADDRESS, grpc::InsecureServerCredentials());
	serverBuilder.RegisterService(&serviceImplementation);
	std::unique_ptr<Server> server(serverBuilder.BuildAndStart());
	std::cout << "Vulnerable server started" << std::endl;
	server->Wait();
}
