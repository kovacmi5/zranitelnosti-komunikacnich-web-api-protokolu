#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <set>
#include <array>
#include <algorithm>
#include <sstream>
#include <cstring>

#define COMMENT_SIGN '#'
#define NUMBER_OF_CONFIG_PARTS 4

constexpr const char* configParts[NUMBER_OF_CONFIG_PARTS] = {
"proto:", "log:", "deny:", "warn:" 
};

constexpr unsigned int hash(const char* data, int size) noexcept
{
	unsigned int constant = 5381;
	
	for(const char* c = data; c < data + size; ++c){
		constant = ((constant << 5) + constant) + (unsigned char)*c;
	}
	
	return constant;
}

class CProtoChecker
{
public:
	CProtoChecker();
	~CProtoChecker();
	
	int check(const char* confFile);
private:

	int checkFile(const char* fileName, const std::set<std::string>& denySet, const std::set<std::string>& warnSet);
	bool createSets(std::set<std::string>& denySet, std::set<std::string>& warnSet);

	std::string readLine(std::istream& stream);
	void writeLine(const std::string& line);

	void trim(std::string& toBeTrimmed);
	void lTrim(std::string& toBeTrimmed);
	void rTrim(std::string& toBeTrimmed);

	std::ifstream m_ConfStream;
	std::string m_ConfString;

	std::ifstream m_ProtoStream;
	std::string m_ProtoString;
	
	std::ofstream m_LogStream;
	//std::string m_LogString;
	
	bool m_Logging = false;
	int m_NumberOfBytes = 0;
};

CProtoChecker::CProtoChecker() = default;
CProtoChecker::~CProtoChecker()
{
	m_ConfStream.close();
	m_ProtoStream.close();
}

int CProtoChecker::checkFile(const char* fileName, const std::set<std::string>& denySet, const std::set<std::string>& warnSet)
{
	std::ifstream protoStream(fileName);
	if(! protoStream.is_open()) {
		std::cout << "error opening proto file" << std::endl;
		return -1;
	}
	bool deny = false;
	bool warn = false;
	std::string head;
	while(protoStream >> head){
		if("message" != head) 
			continue;
			
		protoStream >> head;
		if(-1 == head.find("{")) {protoStream >> head;}
		while(true){
			protoStream >> head;
			if("}" == head) break;
			protoStream >> head;
			for(auto& i: denySet){
				if(-1 != i.find("contains:")){

					if(head == i.substr(10)){
						warn = true;
						std::cout << head << " == " << i.substr(10) << std::endl;
					}
				}
				else if(-1 != i.find("match:")){
					if(-1 != head.find(i.substr(7))){
						warn = true;
						std::cout << head << " contains " << i.substr(7) << std::endl;
					}
				}
				else {std::cout << "fuck off mate: " << i << std::endl;}
			}
			while(protoStream >> head){
				if(-1 != head.find(";")) break;
			}
		}
		
	
	}

	return 1;	
}

int CProtoChecker::check(const char* confFile)
{
	m_ConfStream.open(confFile);
	if(! m_ConfStream.is_open()){
		std::cout << "oops" << std::endl;
		return 0;
	}
	
	std::string head = readLine(m_ConfStream);
	if(0 != head.compare(configParts[0])){
		std::cout << "expected proto files at the beggining (proto:)" << std::endl;
		return -1;
	}

	std::vector<std::string> protoFiles;
	bool toBreak = false;
	while(true){
		head = readLine(m_ConfStream);
		for(int i = 1; i < NUMBER_OF_CONFIG_PARTS; ++i){
			if(configParts[i] == head) {toBreak = true; break;}
			if("" == head){
				std::cout << "WRONG FILE FORMATTING WHILE READING PROTOS" << std::endl;
				return -1;
			}
		}
		if(toBreak)
			break;
			
		protoFiles.emplace_back(head);
	}
	if(configParts[1] == head){
		head = readLine(m_ConfStream);
		m_LogStream.open(head.c_str(), std::ios::app );
		if(m_LogStream.is_open())
			{ m_Logging = true; }
		
	}

	int ret = 1;
	std::set<std::string> denySet;
	std::set<std::string> warnSet;
	bool a = createSets(denySet, warnSet);
	
	for(auto& i: protoFiles){
		int line = checkFile(i.c_str(), denySet, warnSet);
		if(! ret){
			std::cout << "Checking file " << i << " showed error with word at config line " << line << "\n";
			break; 
		}
		ret++;
	}
	
	return ret;
}

bool CProtoChecker::createSets(std::set<std::string>& denySet, std::set<std::string>& warnSet)
{
	std::string head = readLine(m_ConfStream);
	bool toBreak = false;
	while(true)
	{
		switch(hash(head.c_str(), head.size())){
			case hash(configParts[2], strlen(configParts[2])):

				while(true){
					head = readLine(m_ConfStream);
					if(configParts[3] != head && "" != head)
						denySet.insert(head);
					else break;
				}
			case hash(configParts[3], strlen(configParts[3])):
				while(true){
					head = readLine(m_ConfStream);
					if(configParts[2] != head && "" != head)
						denySet.insert(head);
					else break;
				}
			default: toBreak = true; break;
		}
		if(toBreak)
			break;
	}
	return true;
}

std::string CProtoChecker::readLine(std::istream& stream) 
{
	std::string ret;
	char buffer[256] = { 0 };
	while(stream.getline(buffer, 256)){
		ret = buffer;
		m_NumberOfBytes += stream.gcount();
		trim(ret);
	//	std::cout << "reading line: " << ret << std::endl;
		if("" == ret)
			continue;
		if(COMMENT_SIGN != ret.at(0)){
			break;
		}
	}
	
//	std::cout << "reading line: " << ret << wtf << std::endl;
	return ret;
}

void CProtoChecker::writeLine(const std::string& line)
{
	if(! m_Logging) return;
	
	m_LogStream << line << std::endl;
}

void CProtoChecker::trim(std::string& toBeTrimmed)
{
	lTrim(toBeTrimmed);
	rTrim(toBeTrimmed);
}

void CProtoChecker::lTrim(std::string& toBeTrimmed)
{
	auto it = std::find_if(toBeTrimmed.begin(), toBeTrimmed.end(), 
				[](char ch){return !std::isspace<char>(ch, std::locale::classic());});
	
	toBeTrimmed.erase(toBeTrimmed.begin(), it);
}

void CProtoChecker::rTrim(std::string& toBeTrimmed)
{
	auto it = std::find_if(toBeTrimmed.rbegin(), toBeTrimmed.rend(), 
				[](char ch){return !std::isspace<char>(ch, std::locale::classic());});
	
	toBeTrimmed.erase(it.base(), toBeTrimmed.end());
}

int main()
{
	CProtoChecker a;
	a.check("vuln2.config");
}
